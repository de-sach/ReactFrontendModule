const root = "./";
const src = root + "";
const dest = root + "";
const gutil = require("gulp-util");

module.exports = {
  "root": root,
  "publicURL": "foreach.be",
  "devURL": "foreach.local",
  "templates": ["**/*.html"],
  "scss": {
    "src": [
      src + "scss/" + "**/*.scss",
      "!" + src + "node_modules/**/*.scss"
    ],
    "dest": dest + "css/"
  },
  "css": {
    "src": src + (gutil.env.path ? gutil.env.path : "") + "css/**/*.css",
    "lint": src + (gutil.env.path ? gutil.env.path : "") + "css/**/*.css"
  },
  "js": {
    "src": src + "js-src/",
    "lint": [src + "js-src/app/**/*.js", src + "js-src/app/**/*.jsx"],
    "test": src + "js-src/app/**/*.test.*",
    "dest": dest + "js/"
  },
  "svg": {
    "src": src + "svg-src/**",
    "dest": dest + "svg/"
  },
  "images_src": "static/**/*",
  "images_dest": "static/",
};
