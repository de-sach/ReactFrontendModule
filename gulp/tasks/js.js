import gulp from "gulp";
import gutil from "gulp-util";
import config from "../config";
import webpack from "webpack";
import webpackConfig from "../../webpack.config.js";
import del from "del";

const eslint = require("gulp-eslint");

// base config, to be extended
let myConfig = Object.create(webpackConfig);

function clearBuildFolder(callback) {
  callback();

  return del([
    config.js.dest
  ], { "force": true });
}


gulp.task("js:lint", function () {
  console.log("linting " + config.js.lint);

  return gulp.src(config.js.lint)
    .pipe(eslint())
    // eslint.format() outputs the lint results to the console.
    // Alternatively use eslint.formatEach() (see Docs).
    .pipe(eslint.format())
    // To have the process exit with an error code (1) on
    // lint error, return the stream and pipe to failAfterError last.
    .pipe(eslint.failAfterError());
});

gulp.task("js", gulp.series("js:lint", clearBuildFolder, function compile(callback) {
  // if we are production, single compile
  // if we are dev, watch with sourcemaps

  if (gutil.env.watch) {
    console.log("WATCHING");
    myConfig.watch = true;
  } else {
    callback();
  }

  if (gutil.env.production) {
    myConfig.devtool = "";
    myConfig.plugins = myConfig.plugins.concat(
      new webpack.DefinePlugin({
        "process.env": {
          // This has effect on the react lib size
          "NODE_ENV": JSON.stringify("production")
        }
      }),
      new webpack.optimize.UglifyJsPlugin()
    );

  } else {
    myConfig.plugins = myConfig.plugins.concat(
      new webpack.LoaderOptionsPlugin({
        debug: true
      }));
    myConfig.cache = false;
  }

  // run webpack
  return webpack(myConfig, function (err, stats) {
    if (err) throw new gutil.PluginError("webpack:build", err);
    gutil.log("[webpack:build]", stats.toString({
      colors: true
    }));
  });

}));

