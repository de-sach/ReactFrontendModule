import "./scss";
import "./js";
import gulp from "gulp";
import browserSync from "browser-sync";
import config from "../config";
import webpack from "webpack";
import webpackConfig from "../../webpack.config.js";
import gutil from "gulp-util";
import { middleware } from "../../js-src/app/utils/middleware";
import express from "express";

const bundler = webpack(webpackConfig);

// Static server
gulp.task("serve", gulp.series("scss", "js", function () {

  const browserSyncConfig = {};

  browserSyncConfig.server = {
    "notify": true,
    // Customize the Browsersync console logging prefix
    "logPrefix": "FE",
    "baseDir": gutil.env.slicing ? config.dest : "./",
    "path": config.dest + "",
    "middleware": [
      middleware.getTokenMiddleWare(),
      middleware.getRepositoryMiddleware(),
      middleware.createFolderMiddleware(),
      middleware.createDocumentMiddleware(),
      middleware.getFolderMiddleware(),
      middleware.deleteObjectMiddleware(),
      middleware.updateMetadataMiddleware(),
      middleware.searchMiddleware(),
      middleware.getAllMetadataTagsMiddleware(),
      middleware.getFileNameMiddleware()
    ],
    "port":
      3000
  }
  ;

  if (gutil.env.slicing) {
    browserSyncConfig.startPath = "/";
  } else {
    browserSyncConfig.startPath = "/";
    /* the homepage */
  }

  // watch for changes in markup/templates, css
  browserSyncConfig.files = config.templates;
  browserSync.create("My Server").init(browserSyncConfig);

  // gulp.watch(config.scss.src, gulp.series(["scss"]));

}));

// server 2
gulp.task("serve_sach", gulp.series("scss", "js", () => {
  const app = express();
  app.use(express.static("/"));
  app.use(middleware.getTokenMiddleWare());
  app.use(middleware.getTokenMiddleWare());
  app.use(middleware.getRepositoryMiddleware());
  app.use(middleware.createFolderMiddleware());
  app.use(middleware.createDocumentMiddleware());
  app.use(middleware.getFolderMiddleware());
  app.use(middleware.deleteObjectMiddleware());
  app.use(middleware.updateMetadataMiddleware());
  app.use(middleware.searchMiddleware());
  app.use(middleware.getAllMetadataTagsMiddleware());
  app.use(middleware.getFileNameMiddleware());
  app.get("/", (req, res) => {
    res.sendFile("/index.html");
  });
  app.listen(3000);
}));
