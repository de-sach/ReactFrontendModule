describe('My First Test', () => {
  it('Does not do much!', () => {
    expect(true).to.equal(true)
  })
});
describe('My second Test', () => {
  it('open website', () => {
    cy.visit("http://localhost:3000")
  })
});
describe('My third Test', () => {
  it('find content type', () => {
    cy.visit("http://localhost:3000");
    cy.contains('Username');
  })
});
describe('My fourth Test', () => {
  it('try clicking login', () => {
    cy.visit("http://localhost:3000");
    cy.contains('Sign in').click();
  }
});
describe('Login Test', () => {
  it('try logging in', () => {
    login();
  })
});
describe('Add a new folder', () => {
  it("add a new folder",() => {
    login();
    cy.contains("add folder").click();

  })
});

export const login = () => {
  cy.visit("http://localhost:3000");
  cy.get("input#username").type("admin").should("have.value", "admin");
  cy.get("input#password").type("admin").should("have.value", "admin");
  cy.contains("Sign in").click();
};
