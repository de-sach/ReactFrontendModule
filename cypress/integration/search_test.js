import { login } from "./simple_spec";

describe("display search window", () => {
  it("login", () => {
    login();
  });
  it("check for search parts", () => {
    cy.get("input#search").type("enter").should("have.value", "enter");
    cy.get(".m-search__searchType").should("to.contain", "metadata tags");
    cy.contains("Search").click();
  });
  it("check if we get search results", () => {
    cy.get('.m-search__searchedHeader').should("to.contain", "enter");
    cy.get('.m-file > :nth-child(3) > .col-sm-3').click(); // only possible if we have file results
  });
  it("check if the research results contain the wanted tag", () => {
    cy.get('.m-filedetail > :nth-child(2)').should("to.contain", "enter");
  });
});

describe("search by name", () => {
  it("login", () => {
    login();
  });
  it("search by name items", () => {
    cy.get(".m-search__searchType").should("to.contain", "metadata tags");
    cy.get(".m-search__searchType").click().should("to.contain", "name");
    cy.get("input#search").type("newtitle.sh").should("have.value", "newtitle.sh");
    cy.contains("Search").click();
  });
  it("check results", () => {
    cy.get('.m-file > :nth-child(3) > .col-sm-3').should("to.contain", "newtitle.sh"); // only possible if we have file results
  });
  it("check partial fileNames", () => {
    cy.get("input#search").clear().type("title").should("have.value", "title");
    cy.get('.m-file > :nth-child(3) > .col-sm-3').should("to.contain", "title");
  })
});
