describe("Login", () => {
  it("test testing", () => {
    expect(true).to.equal(true);
  });
  it("navigate page", () => {
    cy.visit("http://localhost:3000");
  });
  it("fill in data", () => {
    cy.get("input#username").type("admin").should("have.value", "admin");
    cy.get("input#password").type("admin").should("have.value", "admin");
  });
  it("check login button", () => {
    cy.contains("Sign in");
  });
});
describe("Login success", () => {
  it("login", () => {
    login();
  });
});
describe("Check all required elements", () => {
  it("login", () => {
    login();
  });
  it("check logout button", () => {
    cy.contains("Logout");
  });
  it("check edit users button", () => {
    cy.contains("Edit Users");
  });
  it("check title", () => {
    cy.contains("Welcome to the");
  });
  it("check path", () => {
    cy.contains("root");
  });
  it("check selected items", () => {
    cy.contains("Selected");
  });
  it("check edit options", () => {
    cy.contains("Add Document");
    cy.contains("Add Folder");
    cy.contains("Delete");
  });
  it("check query stuff", () => {
    cy.contains("filtermode");
    cy.contains("name");
    cy.contains("metadata");
  });
});
describe("switch to filter mode", () => {
  it("switch mode", () => {
    cy.contains("filtermode").click();
  });
  it("check items", () => {
    cy.get(".m-filter__itemList");
    cy.get(".m-filter__itemList > :nth-child(1)");
    cy.get(".m-filter__itemList > :nth-child(2)");
    cy.get(".m-filter__itemList > :nth-child(3)");
    cy.get(".m-filter__itemList > :nth-child(4)");
    cy.get(".m-filter__itemList > :nth-child(5)");
    cy.get(".m-filter__itemList > :nth-child(6)");
  });
  it("switch back", () => {
    cy.contains("searchmode").click();
    cy.contains("name");
    cy.contains("metadata");
  });
});


const login = () => {
  cy.visit("http://localhost:3000");
  cy.get("input#username").type("admin").should("have.value", "admin");
  cy.get("input#password").type("admin").should("have.value", "admin");
  cy.contains("Sign in").click();
};
