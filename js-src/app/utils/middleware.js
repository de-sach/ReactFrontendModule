import xml2js from "react-native-xml2js";
import Busboy from "busboy";
import contentManagementRequests from "./oauthContentManagementRequests";
import { AtomPubConstants, middlewareConstants } from "../constants/AtomPubConstants";
import { hex2string } from "./hex2stringconverter";
import { censor } from "./censor";


const ownPrefix = "extcmis";

const tokenMiddleWare = {
  "route": "/token",
  "handle": (req, res) => {
    const body = [];
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      let data = hex2string(body[0].toJSON().data);
      data = JSON.parse(data);
      let responseData = {};
      contentManagementRequests.getAccessToken(data.username, data.password, (success, response) => {
        if (success) {
          responseData = response.data;
          res.writeHead(200, { 'Content-Type': 'text/plain' });
        } else {
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          console.log(response);
        }
        res.write(JSON.stringify(responseData, censor(responseData)));
        res.end();
      });
    });
  }
};

const repositoryMiddleware = {
  "route": middlewareConstants.repositoryUrl,
  "handle": (req, res) => {
    const body = [];
    const finalData = [];
    const children = [];
    let childUrl = null;
    let repositoryName = "";
    // READ REQUEST
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      let data = hex2string(body[0].toJSON().data);
      data = JSON.parse(data);
      let responseData = {};
      // GET REPOSITORY
      contentManagementRequests.getRepositoryRequest(data.url, data.token, (succes, response) => {
        if (succes) {
          responseData = response.data;
        }
        const parseString = xml2js.parseString;
        parseString(responseData, (err, result) => {
          if (!err) {
            responseData = result;
          }
        });
        finalData.push(responseData);
        // GET ROOTFOLDER URL
        if (responseData["app:service"] !== undefined) {
          // console.log(responseData["app:service"]);
          if (responseData["app:service"]["app:workspace"] !== undefined) {
            // console.log("dont mind me");
            // console.log(responseData["app:service"]["app:workspace"]);
            // console.log(responseData["app:service"]["app:workspace"][0]["app:collection"]);
            childUrl = responseData["app:service"]["app:workspace"][0]["app:collection"][0].$.href;
            repositoryName = responseData["app:service"]["app:workspace"][0]["cmisra:repositoryInfo"][0]["cmis:repositoryName"][0];
          }
        }
        finalData.push(childUrl);
        if (childUrl !== null) {
          // GET ROOTFOLDER
          contentManagementRequests.getRepositoryRequest(childUrl, data.token, (childSucces, childResponse) => {
            if (childSucces) {
              responseData = childResponse.data;
              res.writeHead(200, { 'Content-Type': 'text/plain' });
            } else {
              res.writeHead(500, { 'Content-Type': 'text/plain' });
            }
            parseString(responseData, (err, result) => {
              if (!err) {
                responseData = result;
              }
            });
            finalData.push(responseData);
            // GET ROOT FOLDER DATA
            const feed = responseData["atom:feed"];
            const folderTitle = feed["atom:title"][0];
            const rootFolderChildUrl = feed["atom:link"][4].$.href;
            for (const childIndex in feed["atom:entry"]) {
              if (childIndex) {
                // console.log(feed["atom:entry"][childIndex]);
                const child = {
                  "title": feed["atom:entry"][childIndex]["atom:title"][0],
                  "url": feed["atom:entry"][childIndex]["atom:link"][1].$.href, // cmisra id
                  "path": feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyString"][5]["cmis:value"][0],
                  "type": feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyId"][0]["cmis:value"][0]
                };
                if (child.type === "cmis:document") {
                  // check for extcmis:pdf_document
                  const objectType = feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyId"][1]["cmis:value"][0];
                  // console.log(objectType);
                  if (objectType === `${ownPrefix}:pdf_document`) {
                    // add metadata
                    child.type = `${ownPrefix}:pdf_document`;
                    const metadata = feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyString"];
                    for (const index in metadata) {
                      if (index) {
                        if (metadata[index]["cmis:value"] !== undefined) {
                          if (metadata[index].$.propertyDefinitionId === "cmis:description") {
                            child.metadata = metadata[index]["cmis:value"][0];
                          }
                        }
                      }
                    }
                  }
                }
                children.push(child);
              }
            }
            const folder = {
              "folderTitle": folderTitle,
              "childUrl": rootFolderChildUrl,
              "children": children
            };
            const repository = {
              "repositoryName": repositoryName,
              "folder": folder,
              "rootFolderUrl": childUrl
            };
            // console.log(repository);
            res.write(JSON.stringify(repository, censor(repository)));
            res.end();
          });
        }
      });
    });
  }
};

const createFolderMiddleware = {
  "route": middlewareConstants.createFolderUrl,
  "handle": (req, res) => {
    const body = [];
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      let data = hex2string(body[0].toJSON().data);
      data = JSON.parse(data);
      // res.end(data);
      contentManagementRequests.createFolder(data.url, data.token, data.folderName, (succes, response) => {
        if (succes) {
          data = response.data;
          res.writeHead(200, { 'Content-Type': 'text/plain' });
        } else {
          // config, request, response
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          data = response.response;
        }
        let responseData = data;
        /* eslint-disable no-undef */
        const parseString = xml2js.parseString;
        /* eslint-enable no-undef */
        parseString(responseData, (err, result) => {
          if (!err) {
            responseData = result;
          }
        });
        // console.log(responseData);
        res.write(JSON.stringify(responseData, censor(responseData)));
        res.end();
      });
    });
  }
};

const createDocumentMiddleware = {
  "route": "/createDocument",
  "handle": (req, res) => {
    let data = {};
    const file = {};
    const bb = new Busboy({ "headers": req.headers });
    bb.on("file", (fieldName, file1, filename, encoding, mimetype) => {
      file.fileName = filename;
      file.mimeType = mimetype;
      file.data = [];
      file1.on("data", (received) => {
        file.data.push(received);
      });
      file1.on("end", () => {
        console.log("upload done");
      });
    });
    bb.on("field", (name, val) => {
      data[name] = [val];
    });
    bb.on("finish", () => {
      data.file = file;
      console.log("busboy is done");
      contentManagementRequests.createDocument(data, (succes, response) => {
        if (succes) {
          data = response.data;
          res.writeHead(200, { 'Content-Type': 'text/plain' });
        } else {
          res.writeHead(405, { 'Content-Type': 'text/plain' });
          res.write("uploading this content is not allowed\n");
          // config, request, response
          // data = response.response;
        }
        const parseString = xml2js.parseString;
        parseString(data, (err, result) => {
          if (!err) {
            data = result;
          }
        });
        res.write(JSON.stringify(data, censor(data)));
        // res.write("temp data");
        res.end();
      });
    });
    req.pipe(bb);
  }
};

const getFolderMiddleware = {
  "route": middlewareConstants.getFolder,
  "handle": (req, res) => {
    const body = [];
    const finalData = [];
    const children = [];
    // READ REQUEST
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      let data = hex2string(body[0].toJSON().data);
      data = JSON.parse(data);
      let responseData = {};
      // GET FOLDER
      contentManagementRequests.getRepositoryRequest(data.url, data.token, (childSucces, childResponse) => {
        if (childSucces) {
          responseData = childResponse.data;
          res.writeHead(200, { 'Content-Type': 'text/plain' });
        } else {
          res.writeHead(500, { 'Content-Type': 'text/plain' });
        }
        const parseString = xml2js.parseString;
        parseString(responseData, (err, result) => {
          if (!err) {
            responseData = result;
          }
        });
        finalData.push(responseData);
        // GET ROOT FOLDER DATA
        const feed = responseData["atom:feed"];
        const folderTitle = feed["atom:title"][0];
        const rootFolderChildUrl = feed["atom:link"][4].$.href;
        for (const childIndex in feed["atom:entry"]) {
          if (childIndex) {
            // console.log(feed["atom:entry"][childIndex]);
            const child = {
              "title": feed["atom:entry"][childIndex]["atom:title"][0],
              "url": feed["atom:entry"][childIndex]["atom:link"][1].$.href, // cmisra id
              "path": feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyString"][5]["cmis:value"][0],
              "type": feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyId"][0]["cmis:value"][0]
            };
            if (child.type === "cmis:document") {
              // check for extcmis:pdf_document
              const objectType = feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyId"][1]["cmis:value"][0];
              if (objectType === `${ownPrefix}:pdf_document`) {
                // add metadata
                child.type = `${ownPrefix}:pdf_document`;
                const metadata = feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyString"];
                console.log(metadata);
                for (const index in metadata) {
                  if (index) {
                    if (metadata[index]["cmis:value"] !== undefined) {
                      if (metadata[index].$.propertyDefinitionId === "cmis:description") {
                        child.metadata = metadata[index]["cmis:value"][0];
                      }
                    }
                  }
                }
              }
            }
            children.push(child);
          }
        }
        const folder = {
          "folderTitle": folderTitle,
          "childUrl": rootFolderChildUrl,
          "children": children,
          "path": folderTitle
        };
        res.write(JSON.stringify(folder));
        res.end();
      });
    });
  }
};
const deleteObjectMiddleware = {
  "route": middlewareConstants.deleteObject,
  "handle": (req, res) => {
    const body = [];
    // READ REQUEST
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      let data = hex2string(body[0].toJSON().data);
      data = JSON.parse(data);
      contentManagementRequests.deleteObject(data.objectId, data.token, (success, response) => {
        data = response.data;
        if (success) {
          const parseString = xml2js.parseString;
          parseString(data, (err, result) => {
            if (!err) {
              res.writeHead(200, { 'Content-Type': 'text/plain' });
              data = result;
            } else {
              res.writeHead(500, { 'Content-Type': 'text/plain' });
              console.log(result);
            }
          });
          res.write(JSON.stringify(data, censor(data)));
          res.end();
        } else {
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          // return the parameters that give an error
          res.write("computer says no");
          res.end();
        }
      });
    });
  }
};
const updateMetadataMiddleware = {
  "route": middlewareConstants.updateMetadata,
  "handle": (req, res) => {
    const body = [];
    // READ REQUEST
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      let data = hex2string(body[0].toJSON().data);
      data = JSON.parse(data);
      contentManagementRequests.updateMetadata(data.url, data.token, data.metadata, (success, response) => {
        if (success) {
          data = response;
          res.writeHead(200, { 'Content-Type': 'text/plain' });
          res.write(JSON.stringify(data, censor(data)));
          res.end();
        } else {
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          // return the parameters that give an error
          res.write(JSON.stringify(data, censor(data)));
          res.end();
        }
      });
    });
  }
};
const searchMiddleware = {
  "route": middlewareConstants.search,
  "handle": (req, res) => {
    const body = [];
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      let data = hex2string(body[0].toJSON().data);
      data = JSON.parse(data);
      const search = data.search;
      contentManagementRequests.search(data.search, data.token, data.type, (success, response) => {
        if (success) {
          data = response.data;

          const parseString = xml2js.parseString;
          parseString(data, (err, result) => {
            if (!err) {
              data = result;
            }
            console.log(data);
          });
          const children = [];
          console.log(data);
          const feed = data["atom:feed"];
          const folderTitle = search;
          const rootFolderChildUrl = feed["atom:link"][0].$.href;
          for (const childIndex in feed["atom:entry"]) {
            if (childIndex) {
              console.log(feed["atom:entry"][childIndex]);
              const child = {
                "title": feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyString"][0]["cmis:value"][0],
                "url": AtomPubConstants.baseUrl, // TODO: fix this
                "path": feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyString"][5]["cmis:value"][0],
                "type": feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyId"][0]["cmis:value"][0]
              };
              if (child.type === "cmis:document") {
                // check for extcmis:pdf_document
                const objectType = feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyId"][1]["cmis:value"][0];
                if (objectType === `${ownPrefix}:pdf_document`) {
                  // add metadata
                  child.type = `${ownPrefix}:pdf_document`;
                  const metadata = feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyString"];
                  for (const index in metadata) {
                    if (index) {
                      if (metadata[index]["cmis:value"] !== undefined) {
                        if (metadata[index].$.propertyDefinitionId === "cmis:description") {
                          child.metadata = metadata[index]["cmis:value"][0];
                        }
                      }
                    }
                  }
                }
              }
              children.push(child);
            }
          }
          const folder = {
            "folderTitle": folderTitle,
            "childUrl": rootFolderChildUrl,
            "children": children,
            "path": folderTitle
          };
          res.writeHead(200, { 'Content-Type': 'text/plain' });
          res.write(JSON.stringify(folder, censor(folder)));
          res.end();
        } else {
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          res.write("{computer doesn't seem to like you:true}");
          res.end();
        }
      });
    });
  }
};
const allMetadataTagsMiddleware = {
  "route": middlewareConstants.getAllMetadataUrl,
  "handle": (req, res) => {
    const body = [];
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      const metadataTags = [];
      let data = hex2string(body[0].toJSON().data);
      data = JSON.parse(data);
      contentManagementRequests.getAllMetadataTags(data.token, (success, response) => {
        if (success) {
          data = response.data;
          const parseString = xml2js.parseString;
          parseString(data, (err, result) => {
            if (!err) {
              data = result;
            }
          });
          const feed = data["atom:feed"];
          for (const childIndex in feed["atom:entry"]) {
            if (childIndex) {
              // console.log(feed["atom:entry"][childIndex]);
              const objectType = feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyId"][1]["cmis:value"][0];
              if (objectType === `${ownPrefix}:pdf_document`) {
                // add metadata
                const metadata = feed["atom:entry"][childIndex]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyString"];
                for (const index in metadata) {
                  if (index) {
                    if (metadata[index]["cmis:value"] !== undefined) {
                      if (metadata[index].$.propertyDefinitionId === "cmis:description") {
                        let tagsFromItem = metadata[index]["cmis:value"][0];
                        tagsFromItem = tagsFromItem.slice(1, tagsFromItem.length - 1).split(",");
                        for (const itemIndex in tagsFromItem) {
                          if (true) {
                            if (metadataTags.indexOf(tagsFromItem[itemIndex]) === -1) {
                              metadataTags.push(tagsFromItem[itemIndex]);
                            }
                          }
                        }
                      }
                    }
                  }

                }
              }
            }
          }
          res.writeHead(200, { 'Content-Type': 'text/plain' });
          res.write(JSON.stringify({ "metadata": metadataTags }));
          res.end();
        } else {
          console.log(response);
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          res.write("{computer doesn't seem to like you:true}");
          res.end();
        }
      });
    });
  }
};

const fileNameMiddleware = {
  "route": middlewareConstants.updateFileName,
  "handle": (req, res) => {
    const body = [];
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      let data = hex2string(body[0].toJSON().data);
      data = JSON.parse(data);
      contentManagementRequests.updateFileName(data.url, data.token, data.name, (success) => {
        if (success) {
          res.writeHead(200, { 'Content-Type': 'text/plain' });
          res.write(`{title: ${data.name}}`);
          res.end();
        } else {
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          res.write("{computer doesn't seem to like you:true}");
          res.end();
        }
      });

    });
  }
};


export const middleware = {
  getTokenMiddleWare() {
    return tokenMiddleWare;
  },
  getRepositoryMiddleware() {
    return repositoryMiddleware;
  },
  createFolderMiddleware() {
    return createFolderMiddleware;
  },
  createDocumentMiddleware() {
    return createDocumentMiddleware;
  },
  getFolderMiddleware() {
    return getFolderMiddleware;
  },
  deleteObjectMiddleware() {
    return deleteObjectMiddleware;
  },
  updateMetadataMiddleware() {
    return updateMetadataMiddleware;
  },
  searchMiddleware() {
    return searchMiddleware;
  },
  getAllMetadataTagsMiddleware() {
    return allMetadataTagsMiddleware;
  },
  getFileNameMiddleware() {
    return fileNameMiddleware;
  }
};
