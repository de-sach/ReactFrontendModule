import axios from 'axios';
// import connect from 'connect';
// import bodyParser from 'body-parser';
// import http from "http";
import { AtomPubConstants, middlewareConstants, Oauth2TokenConstants } from "../constants/AtomPubConstants";
import { stringReplacer } from "../utils/stringReplacer";


const contentManagementRequests = {
  Login(url, username, password, callback) {
    const data = {
      "username": username,
      "password": password
    };
    axios.post(url, data)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  getAccessToken(userName, password, callback) {
    const url = Oauth2TokenConstants.baseUrl;
    const data = "";
    const Oauth2Data = Oauth2TokenConstants.parameters;
    Oauth2Data.username = userName;
    Oauth2Data.password = password;
    const options = {
      "headers": {
        'X-Requested-With': 'XMLHttpRequest'
      },
      "xsrfCookieName": 'XSRF-TOKEN',
      "xsrfHeaderName": 'X-XSRF-TOKEN',
      "params": Oauth2Data
    };
    axios.post(url, data, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
    // localhost:8080/oauth/token?client_id=formula&client_secret=RHVSR4fTWTScB5Pz&grant_type=password&username=admin&password=admin
  },
  getRepositoryRequest(url, token, callback) {
    const options = {
      "params": {
        'access_token': token
      }
    };
    axios.get(url, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  getRepositoryMiddleware(url, token, callback) {
    const data = {
      "url": AtomPubConstants.baseUrl,
      "token": token
    };
    axios.post(url, data)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  createFolderMiddleware(url, folderName, token, callback) {
    const data = {
      "url": url,
      "folderName": folderName,
      "token": token
    };
    axios.post(middlewareConstants.createFolderUrl, data)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  createFolder(url, token, foldername, callback) {
    let data = AtomPubConstants.createFolderTemplate;
    data = stringReplacer(data, "template1", foldername);
    const options = {
      "headers": {
        "Content-Type": "application"
      },
      "params": {
        'access_token': token
      }
    };
    axios.post(url, data, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  createDocumentMiddleware(url, fileName, content, token, callback) {
    // let base64Content = null;
    // getBase64(content, (succes, base64) => {
    //   if (succes) {
    //     base64Content = base64;
    //   }
    // const data = {
    //   "url": url,
    //   "fileName": fileName,
    //   "content": base64Content,
    //   "token": token
    // };
    const formData = new FormData();
    formData.append("url", url);
    formData.append("fileName", fileName);
    formData.append("file", content);
    formData.append("token", token);
    axios.post(middlewareConstants.createDocumentUrl, formData)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
    // });
  },
  createDocumentsMiddleware(url, files, token, callback) {
    for (let count = 0; count < files.length; count += 1) {
      this.createDocumentMiddleware(url, files[count].name, files[count], token, callback);
    }
  },
  createDocument(originalData, callback) {
    // const formData = new FormData();
    const url = originalData.url[0];
    const fileName = originalData.fileName[0];
    const token = originalData.token[0];
    const mimeType = originalData.file.mimeType;
    let fileData = "";
    for (let i = 0; i < originalData.file.data.length; i += 1) {
      const buf = originalData.file.data[i];
      fileData += buf.toString("base64");
    }
    // let base64Content = null;
    // const base64Content = Buffer.from(fileData).toString("base64");
    let documentType = AtomPubConstants.documentTypes.document;
    let newFileName = fileName;
    if (fileName.split(".").length === 1) {
      newFileName = fileName + ".txt";
    }
    let extension = newFileName.split(".")[1];
    extension = extension.toLowerCase();
    if (extension === "pdf") {
      documentType = AtomPubConstants.documentTypes.pdf;
    }
    let data = AtomPubConstants.createDocumentTemplate;
    data = stringReplacer(data, "template1", newFileName);
    data = stringReplacer(data, "template2", fileData);
    data = stringReplacer(data, "template3", mimeType);
    data = stringReplacer(data, "template4", documentType);
    data = stringReplacer(data, "template5", "[pdf, document]");
    const options = {
      "headers": {
        "Content-Type": "application"
      },
      "params": {
        'access_token': token
      }
    };
    axios.post(url, data, options)
      .then((response) => {
        if (documentType === AtomPubConstants.documentTypes.pdf) {
          callback(true, response);
        } else {
          callback(true, response);
        }
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  getFolderMiddleware(url, token, callback) {
    const data = {
      "url": url,
      "token": token
    };
    axios.post(middlewareConstants.getFolder, data)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  deleteObjectMiddleware(objectId, token, callback) {
    const data = {
      "objectId": objectId,
      "token": token
    };
    axios.post(middlewareConstants.deleteObject, data)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  deleteObject(objectId, token, callback) {
    const options = {
      "headers": {
        "Content-Type": "application"
      },
      "params": {
        'access_token': token
      }
    };
    let url = AtomPubConstants.baseUrl;
    url = `${url}entry?id=${objectId}`;
    axios.delete(url, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  updateMetadataMiddleware(metadataString, fileUrl, token, callback) {
    const data = {
      "url": fileUrl,
      "token": token,
      "metadata": metadataString
    };
    axios.post(middlewareConstants.updateMetadata, data)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  updateMetadata(url, token, metadata, callback) {
    let data = AtomPubConstants.updateDocumentTemplate;
    data = stringReplacer(data, "template", metadata);
    const options = {
      "headers": {
        "Content-Type": "application"
      },
      "params": {
        'access_token': token
      }
    };
    axios.put(url, data, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((error) => {
        callback(false, error);
      });
  },
  searchMiddleware(search, token, type, callback) {
    const data = {
      "search": search,
      "token": token,
      "type": type
    };
    axios.post(middlewareConstants.search, data)
      .then((response) => {
        callback(true, response);
      })
      .catch((err) => {
        callback(false, err);
      });
  },
  search(search, token, type, callback) {
    let data = "";
    let newSearch = search;
    if (type === "name") {
      data = AtomPubConstants.searchNameTemplate;
    } else {
      data = AtomPubConstants.searchTagTemplate;
      newSearch = `%${search}%`;
    }
    data = stringReplacer(data, "template", newSearch);
    const options = {
      "headers": {
        "Content-Type": "application"
      },
      "params": {
        'access_token': token
      }
    };
    axios.post(AtomPubConstants.searchUrl, data, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((err) => {
        callback(false, err);
      });
  },
  getAllMetadataTagsMiddleware(token, callback) {
    const options = {
      "headers": {
        "Content-Type": "application"
      },
      "params": {
        'access_token': token
      }
    };
    const data = {
      "token": token
    };
    axios.post(middlewareConstants.getAllMetadataUrl, data, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((err) => {
        callback(false, err);
      });
  },
  getAllMetadataTags(token, callback) {
    const options = {
      "headers": {
        "Content-Type": "application"
      },
      "params": {
        'access_token': token
      }
    };
    const data = AtomPubConstants.getAllMetadataTagsTemplate;
    axios.post(AtomPubConstants.searchUrl, data, options, callback)
      .then((response) => {
        callback(true, response);
      })
      .catch((err) => {
        callback(false, err);
      });
  },
  updateFileNameMiddleware(url, token, name, callback) {
    const data = {
      "url": url,
      "name": name,
      "token": token
    };
    const options = {
      "headers": {
        "Content-Type": "application"
      }
    };
    axios.post(middlewareConstants.updateFileName, data, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((err) => {
        callback(false, err);
      });
  },
  updateFileName(url, token, name, callback) {
    let data = AtomPubConstants.updateFileNameTemplate;
    data = stringReplacer(data, "template", name);
    const options = {
      "headers": {
        "Content-Type": "application"
      },
      "params": {
        'access_token': token
      }
    };
    axios.put(url, data, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((err) => {
        callback(false, err);
      });
  }
};

export default contentManagementRequests;
