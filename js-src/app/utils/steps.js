// we add "exceptions" to navigation as previousID or nextID properties in this list;
// if no such property exists, back or forward navigation is based on the index in this list.

export const stepList = [
  { "index": 0, "ID": "step1" },
  { "index": 1, "ID": "step2" },
  { "index": 2, "ID": "step3" }
];

export const findStep = (step) => {
  let matchedStep = null;
  let index = 0;

  const returnValue = (() => {
    matchedStep = stepList.find(loopedStep => loopedStep.ID === step);
    index = matchedStep.index;
    return matchedStep;
  })();

  returnValue.next = () => {
    index += 1;
    return stepList[index] || stepList[index - 1];
  };

  returnValue.previous = () => {
    index -= 1;
    return stepList[index] || stepList[index + 1];
  };

  return returnValue;
};
