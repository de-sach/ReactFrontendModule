export const required = (value) => {
  let result;
  if (value === "") {
    result = "required field!";
  }
  return result;
};

export function validateAllFields(allFields) {
  const errors = {};

  // validation order matters :last validator that returns invalid will "win"
  const validateRules = {
    "FIELD1": [required]
  };


  Object.entries(allFields).forEach(([formName, values]) => {
    const formValue = values[formName];

    // has validation rules
    if (validateRules[formName]) {
      // run each validator on formValue
      let fieldError = null;

      validateRules[formName].forEach((validator) => {
        if (fieldError) return;
        fieldError = validator(formValue, values);
      });

      if (fieldError) {
        // there was an error...
        errors[formName] = fieldError;
      }
    }
  });

  return errors;
}
