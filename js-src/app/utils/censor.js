export const censor = (censored) => {
  let i = 0;
  return function (key, value) {
    if (i !== 0 && typeof (censored) === 'object' && typeof (value) === 'object' && censored === value) {
      return '[Circular]';
    }
    if (i >= 29) { // seems to be a harded maximum of 30 serialized objects?
      return '[Unknown]';
    }
    i += 1; // so we know we aren't using the original object anymore
    return value;
  };
};
