export const hex2string = (hex) => {
  let str = '';
  for (let i = 0; i < hex.length; i += 1) {
    str += String.fromCharCode(hex[i]);
  }
  return str;
};
