/* eslint-disable guard-for-in */
function rewriteProperties(obj) {
  const newObject = {};
  if (typeof obj !== "object") {
    // console.log(`obj is not object, ${obj}`);
  } else {
    // console.log(obj);
    for (const prop in obj) {
      if (prop.startsWith("cmis")) {
        const newProp = prop.replace(":", "");
        // console.log(newProp);
        newObject[newProp] = obj[prop];
      }
    }
  }
  // console.log(newObject);
  return newObject;
}

export { rewriteProperties };

/* eslint-enable guard-for-in */
