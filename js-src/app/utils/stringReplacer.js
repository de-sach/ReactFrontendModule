export const stringReplacer = (str, fnd, replace) => {
  return str.replace(new RegExp(fnd, 'g'), replace);
};
