export const getBase64 = (file, callback) => {
  const reader = new window.FileReader(); // non webpack: new FileReader
  reader.readAsDataURL(file);
  reader.onload = () => {
    callback(true, reader.result);
  };
  reader.onerror = (error) => {
    callback(false, error);
  };
};
