import axios from 'axios';
import cmisConst from '../constants/BrowserConstants';

let un = "";
let pw = "";

const contentManagementRequests = {
  Login(url, uname, pword, callback) {
    un = uname;
    pw = pword;
    axios.get(url, {
      "WithCredentials": true,
      "headers": {
        'X-Requested-With': 'XMLHttpRequest'
      },
      "auth": {
        'username': uname,
        'password': pword
      },
      "xsrfCookieName": 'XSRF-TOKEN',
      "xsrfHeaderName": 'X-XSRF-TOKEN'
    })
      .then((response) => {
        // console.log(response);
        if (response.status === 200) { // ok
          // console.log("logged in");
          callback(response);
        } else {
          console.log("login error");
        }
      })
      .catch((err) => {
        console.log(err);
        switch (err.response.status) {
          case '403':
            console.log("permissions denied");
            break;
          default:
            console.log(`${err}`);
        }
        // console.log(`error type: ${err.response.status}`);
      });
  },
  getRootFolder(rootFolderUrl, callback) {
    // console.log(rootFolderUrl);
    axios.get(rootFolderUrl, {
      "WithCredentials": true,
      "headers": {
        'X-Requested-With': 'XMLHttpRequest'
      },
      "auth": {
        'username': un,
        'password': pw
      },
      "xsrfCookieName": 'XSRF-TOKEN',
      "xsrfHeaderName": 'X-XSRF-TOKEN'
    }).then((response) => {
      // console.log(response);
      callback(true, response);
    })
      .catch((err) => {
        console.log(err);
        callback(false, "repository could not be reached");
      });
  },
  uploadFile(folderUrl, file, callback) {
    const createDocument = cmisConst.createDocument;
    const data = new FormData();
    for (const prop in createDocument) {
      if (prop) {
        data.set(prop, createDocument[prop]);
        // console.log(prop);
        // console.log(createDocument.prop);
        // console.log(createDocument[prop]);
      }
    }
    console.log(data);
    data.set("propertyValue[1]", file.name);
    data.set("content", file);
    // console.log(data.content);
    const options = {
      "WithCredentials": true,
      "headers": {
        'X-Requested-With': 'XMLHttpRequest'
      },
      "auth": {
        'username': un,
        'password': pw
      },
      "xsrfCookieName": 'XSRF-TOKEN',
      "xsrfHeaderName": 'X-XSRF-TOKEN'
    };
    axios.post(folderUrl, data, options)
      .then((response) => {
        // console.log(response);
        callback(response);
      })
      .catch((err) => {
        console.log(err);
        callback("repository could not be reached");
      });
  },
  uploadFiles(folderUrl, files, callback) {
    /* eslint-disable */
    for (let count = 0; count < files.length; count++) {
      this.uploadFile(folderUrl, files[count], callback);
    }
    /* eslint-enable */
  },
  createFolder(folderUrl, folderName, callback) {
    const createFolder = cmisConst.createFolder;
    const params = createFolder;
    params["propertyValue[1]"] = folderName;
    const options = {
      "WithCredentials": true,
      "headers": {
        'X-Requested-With': 'XMLHttpRequest'
      },
      "auth": {
        'username': un,
        'password': pw
      },
      "xsrfCookieName": 'XSRF-TOKEN',
      "xsrfHeaderName": 'X-XSRF-TOKEN',
      "params": params
    };
    const data = "test";
    axios.post(folderUrl, data, options)
      .then((response) => {
        // console.log(response);
        callback(response);
      })
      .catch((err) => {
        console.log(err);
        callback("repository could not be reached");
      });
  },
  deleteObject(url, objectId, callback) {
    const objectUrl = `${url}?objectId=${objectId}`;
    const params = cmisConst.deleteItem;
    const data = "";
    const options = {
      "WithCredentials": true,
      "headers": {
        'X-Requested-With': 'XMLHttpRequest'
      },
      "auth": {
        'username': un,
        'password': pw
      },
      "xsrfCookieName": 'XSRF-TOKEN',
      "xsrfHeaderName": 'X-XSRF-TOKEN',
      "params": params
    };
    axios.post(objectUrl, data, options)
      .then((response) => {
        callback(true, response);
      })
      .catch((err) => {
        callback(false, err);
      });
  }
  // ,getFile(url, objectId, callback) {
  //   const params = {};
  //   params.objectId = objectId;
  //   const options = {
  //     "auth": {
  //       'username': un,
  //       'password': pw
  //     },
  //     "xsrfCookieName": 'XSRF-TOKEN'
  //     "xsrfHeaderName": 'X-XSRF-TOKEN',
  //     "params": params
  //   };
  //   axios.get(url, options)
  //     .then((response) => {
  //       callback(true, response);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //       callback(false, err);
  //     });
  // }
};

export default contentManagementRequests;
