/* eslint-disable no-console */
import { createCookie, eraseCookie, readCookie } from "./cookie";

if (window.location.search.length > 0) {
  if (window.location.search.match(/[?&]debugMode=true/) && console.debug) {
    console.debug("Turning debug mode on and creating cookie");
    createCookie("debugMode", "true", 31);
  } else if (window.location.search.match(/[?&]debugMode=false/) && console.debug) {
    console.debug("Deleting debug cookie");
    eraseCookie("debugMode");
  }
}

// IE10 polyfill window.location.origin
if (!window.location.origin) {
  window.location.origin = `${window.location.protocol}//${window.location.hostname}${window.location.port ? `:${window.location.port}` : ""}`;
}

const cookieValue = readCookie("debugMode");
const loggerEnabled = (cookieValue !== null || window.location.origin.match(/localhost/g));

function log(debugText) {
  if (loggerEnabled) {
    console.log(`Logger ${new Date().toLocaleString()} : ${debugText}`);
  }
}

export default ({

  "debug": function (debugText) {
    if (loggerEnabled && console.debug) {
      console.debug(`Logger ${new Date().toLocaleString()} : ${debugText}`);
    }
  },

  "log": log,

  "warn": function (debugText) {
    if (loggerEnabled && console.warn) {
      console.warn(`Logger ${new Date().toLocaleString()} : ${debugText}`);
    }
  },

  "error": function (debugText) {
    if (loggerEnabled && console.error) {
      console.error(`Logger ${new Date().toLocaleString()} : ${debugText}`);
    }
  }
});
