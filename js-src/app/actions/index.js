export const GOTO_NEXT_STEP = "GOTO_NEXT_STEP";
export const GOTO_PREVIOUS_STEP = "GOTO_PREVIOUS_STEP";
export const GOTO_STEP = "GOTO_STEP";
export const LOGIN = "LOGIN";
export const UPDATEREPO = "UPDATE_REPOSITORY";
export const UPDATE_CURRENT_FOLDER = "UPDATE_FOLDER";
export const ADD_TO_SELECTED_ITEMS = "ADD_TO_SELECTED_ITEMS";
export const REMOVE_FROM_SELECTED_ITEMS = "REMOVE_FROM_SELECTED_ITEMS";
export const CLEAR_SELECTED_ITEMS = "CLEAR_SELECTED_ITEMS";
export const STORE_TOKEN = "STORE_TOKEN";
export const UPDATE_CURRENT_PATH = "UPDATE_CURRENT_PATH";
export const ADD_TO_METADATA = "ADD_TO_METADATA";
export const REMOVE_FROM_METADATA = "REMOVE_FROM_METADATA";
export const EDIT_IN_METADATA = "EDIT_IN_METADATA";
export const SET_ALL_METADATA = "SET_ALL_METADATA";
export const SET_FILE_METADATA = "SET_FILE_METADATA";

export function gotoNextStep() {
  return {
    "type": GOTO_NEXT_STEP
  };
}

export function gotoPreviousStep() {
  return {
    "type": GOTO_PREVIOUS_STEP
  };
}

export function gotoStep(stepNumber) {
  return {
    "type": GOTO_STEP,
    stepNumber
  };
}

export function setLoginState(status) {
  return {
    "type": LOGIN,
    status
  };
}

export function updateRepository(repository) {
  return {
    "type": UPDATEREPO,
    repository
  };
}

export function setCurrentFolder(folder) {
  return {
    "type": UPDATE_CURRENT_FOLDER,
    folder
  };
}

export function addToSelectedItems(item) {
  return {
    "type": ADD_TO_SELECTED_ITEMS,
    item
  };
}

export function removeFromSelectedItems(item) {
  return {
    "type": REMOVE_FROM_SELECTED_ITEMS,
    item
  };
}

export function clearSelectedItems() {
  return {
    "type": CLEAR_SELECTED_ITEMS
  };
}

export function storeToken(token) {
  return {
    "type": STORE_TOKEN,
    token
  };
}

export function setPath(folder) {
  return {
    "type": UPDATE_CURRENT_PATH,
    folder
  };
}

export function addToMetadata(fileId, tag) {
  return {
    "type": ADD_TO_METADATA,
    fileId,
    tag
  };
}

export function removeFromMetadata(fileId, tag) {
  return {
    "type": REMOVE_FROM_METADATA,
    fileId,
    tag
  };
}

export function editInMetadata(fileId, original, newTag) {
  return {
    "type": EDIT_IN_METADATA,
    fileId,
    original,
    newTag
  };
}

export function setAllMetadata(metadata) {
  return {
    "type": SET_ALL_METADATA,
    metadata
  };
}

export function setFileMetadata(fileId, tagList) {
  return {
    "type": SET_FILE_METADATA,
    fileId,
    tagList
  };
}
