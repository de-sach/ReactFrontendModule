import React, { Component } from "react";
import Field from "../components/Field.jsx";

class Step1 extends Component {
  render() {
    return (
      <div className="step1">
        This is Step 1.
        <Field component="input" name="FIELD1" />
      </div>
    );
  }
}

export default Step1;
