import React, { Component } from "react";

class Step2 extends Component {
  render() {
    return (
      <div className="step2">
        This is Step 2.
      </div>
    );
  }
}

export default Step2;
