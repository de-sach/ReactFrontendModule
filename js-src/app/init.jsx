import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import AppReducer from "./reducers/index";
import AppContainer from "./containers/AppContainer";
import LoginContainer from "./containers/LoginContainer";
// import { configure } from "./serverConfig/serverConfig";

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);
let devToolsReference;
if (process.env.NODE_ENV !== "production") {
  devToolsReference = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
}

const appStore = createStoreWithMiddleware(AppReducer, devToolsReference);

// configure.configureServer();

const renderComponent = (Component) => {
  render(
    <Provider store={appStore}>
      <Component />
    </Provider>,
    document.getElementById("content")
  );
};

// renderComponent(AppContainer);
renderComponent(LoginContainer);

// hot module reloading logic
// see https://github.com/gaearon/react-hot-loader/tree/master/docs#migration-to-30
if (module.hot) {
  module.hot.accept("./containers/AppContainer", () => {
    render(AppContainer);
  });
}
