import { connect } from "react-redux";
import { setLoginState, updateRepository, setCurrentFolder } from "../actions";
import Repository from "../components/Explorer/Repository";

const mapStateToProps = state => ({
  "loginState": state.loginState.loginState,
  "repository": state.repositoryState.repository
});

const mapDispatchToProps = dispatch => ({
  "setLoginState": state => dispatch(setLoginState(state)),
  "updateRepository": repository => dispatch(updateRepository(repository)),
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder))
});

export default connect(mapStateToProps, mapDispatchToProps)(Repository);
