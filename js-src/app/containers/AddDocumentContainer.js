import { connect } from "react-redux";
import AddDocument from "../components/Explorer/EditOptions/AddDocument";
import { setCurrentFolder } from "../actions";

const mapStateToProps = state => ({
  "token": state.loginState.token.access_token,
  "folder": state.repositoryState.folder,
  "rootFolderUrl": state.repositoryState.repository.rootFolderUrl
});
const mapDispatchToProps = dispatch => ({
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddDocument);
