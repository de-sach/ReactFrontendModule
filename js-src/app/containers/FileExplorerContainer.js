import { connect } from "react-redux";
import FileExplorer from "../components/Explorer/FileExplorer";
import { clearSelectedItems, setCurrentFolder, setPath } from "../actions";

const mapStateToProps = state => ({
  "folder": state.repositoryState.folder,
  "rootFolderUrl": state.repositoryState.repository.rootFolderUrl,
  "token": state.loginState.token.access_token
});
const mapDispatchToProps = dispatch => ({
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder)),
  "setCurrentPath": folder => dispatch(setPath(folder)),
  "clearSelectedItems": () => dispatch(clearSelectedItems())
});

export default connect(mapStateToProps, mapDispatchToProps)(FileExplorer);
