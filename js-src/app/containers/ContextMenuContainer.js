import { connect } from "react-redux";
import { setCurrentFolder } from "../actions";
import FileContextMenu from "../components/Explorer/ContextMenu/FileContextMenu";

const mapStateToProps = state => ({
  "folder": state.repositoryState.folder,
  "token": state.loginState.token.access_token
});
const mapDispatchToProps = dispatch => ({
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder))
});
export default connect(mapStateToProps, mapDispatchToProps)(FileContextMenu);
