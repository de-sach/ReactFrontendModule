import { connect } from "react-redux";
import LoginPage from "../components/Login/Login";
import { setAllMetadata, setCurrentFolder, setLoginState, setPath, storeToken, updateRepository } from "../actions";

const mapStateToProps = state => ({
  "loginState": state.loginState.loginState,
  "repository": state.repositoryState.repository,
  "token": state.loginState.token
});

const mapDispatchToProps = dispatch => ({
  "setLoginState": state => dispatch(setLoginState(state)),
  "updateRepository": repository => dispatch(updateRepository(repository)),
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder)),
  "storeToken": token => dispatch(storeToken(token)),
  "setPath": folder => dispatch(setPath(folder)),
  "setAllMetadata": metadata => dispatch(setAllMetadata(metadata))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
