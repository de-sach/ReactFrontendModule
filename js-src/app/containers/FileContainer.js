import { connect } from "react-redux";
import File from "../components/Explorer/File";
import { addToSelectedItems, removeFromSelectedItems } from "../actions";

const mapStateToProps = state => ({
  "selectedItems": state.repositoryState.selectedItems
});

const mapDispatchToProps = dispatch => ({
  "addToSelected": file => dispatch(addToSelectedItems(file)),
  "removeFromSelected": file => dispatch(removeFromSelectedItems(file))
});

export default connect(mapStateToProps, mapDispatchToProps)(File);
