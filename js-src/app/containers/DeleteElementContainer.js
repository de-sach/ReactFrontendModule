import { connect } from "react-redux";
import DeleteElement from "../components/Explorer/EditOptions/DeleteElement";
import { clearSelectedItems, setCurrentFolder } from "../actions";

const mapStateToProps = state => ({
  "selectedItems": state.repositoryState.selectedItems,
  "folder": state.repositoryState.folder,
  "token": state.loginState.token.access_token
});
const mapDispatchToProps = dispatch => ({
  "clearSelectedItems": () => dispatch(clearSelectedItems()),
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder))
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteElement);
