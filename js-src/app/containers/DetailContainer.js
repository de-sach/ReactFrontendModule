import { connect } from "react-redux";
import Detail from "../components/Explorer/Details/Detail";
import { addToMetadata, removeFromMetadata, setFileMetadata } from "../actions";

const mapStateToProps = state => ({
  "token": state.loginState.token.access_token,
  "metadataFileList": state.metadata.metadataFileList
});
const mapDispatchToProps = dispatch => ({
  "addToMetadata": (fileId, tag) => dispatch(addToMetadata(fileId, tag)),
  "removeFromMetadata": (fileId, tag) => dispatch(removeFromMetadata(fileId, tag)),
  "setFileMetadata": (fileId, tagList) => dispatch(setFileMetadata(fileId, tagList))
});

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
