import { connect } from "react-redux";
import Folder from "../components/Explorer/Folder";
import { addToSelectedItems, removeFromSelectedItems } from "../actions";

const mapStateToProps = null;
const mapDispatchToProps = dispatch => ({
  "addToSelected": folder => dispatch(addToSelectedItems(folder)),
  "removeFromSelected": folder => dispatch(removeFromSelectedItems(folder))
});

export default connect(mapStateToProps, mapDispatchToProps)(Folder);
