import { connect } from "react-redux";
import Path from "../components/Explorer/Path";
import { setCurrentFolder, setPath } from "../actions";

const mapStateToProps = state => ({
  "path": state.repositoryState.path,
  "token": state.loginState.token.access_token
});
const mapDispatchToProps = dispatch => ({
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder)),
  "setCurrentPath": folder => dispatch(setPath(folder))
});

export default connect(mapStateToProps, mapDispatchToProps)(Path);
