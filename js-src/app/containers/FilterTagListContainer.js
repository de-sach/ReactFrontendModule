import { connect } from "react-redux";
import { setCurrentFolder } from "../actions";
import FilterTagList from "../components/Explorer/Filter/FilterTagList";

const mapStateToProps = state => ({
  "token": state.loginState.token.access_token,
  "metadata": state.metadata.allMetadataList
});

const mapDispatchToProps = dispatch => ({
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder))
});

export default connect(mapStateToProps, mapDispatchToProps)(FilterTagList);
