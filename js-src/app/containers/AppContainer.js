import { connect } from "react-redux";
import App from "../components/App.jsx";
import { gotoNextStep, gotoPreviousStep, gotoStep } from "../actions/index";

// Map Redux store to component props
const mapStateToProps = state => ({
  "currentStep": state.pageState.currentStep
});

// map Redux dispatch events to component props
const mapDispatchToProps = dispatch => ({
  "gotoNextStep": () => {
    dispatch(gotoNextStep());
  },
  "gotoPreviousStep": () => {
    dispatch(gotoPreviousStep());
  },
  "gotoStep": stepNumber => dispatch(gotoStep(stepNumber))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
