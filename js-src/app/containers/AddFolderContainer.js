import { connect } from "react-redux";
import AddFolder from "../components/Explorer/EditOptions/AddFolder";
import { setCurrentFolder } from "../actions";

const mapStateToProps = state => ({
  "folder": state.repositoryState.folder,
  "token": state.loginState.token.access_token,
  "rootFolderUrl": state.repositoryState.repository.rootFolderUrl
});
const mapDispatchToProps = dispatch => ({
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddFolder);
