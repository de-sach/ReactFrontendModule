import { connect } from "react-redux";
import MetadataTag from "../components/Explorer/Details/MetadataTag";
import { addToMetadata, editInMetadata, removeFromMetadata } from "../actions";

const mapStateToProps = state => ({
  "metadata": state.metadata.metadataFileList
});
const mapDispatchToProps = dispatch => ({
  "addToMetadata": (fileId, tag) => {
    dispatch(addToMetadata(fileId, tag));
  },
  "removeFromMetadata": (fileId, tag) => {
    dispatch(removeFromMetadata(fileId, tag));
  },
  "editInMetadata": (fileId, originalTag, newTag) => {
    dispatch(editInMetadata(fileId, originalTag, newTag));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(MetadataTag);
