import { connect } from "react-redux";
import FileDetails from "../components/Explorer/Details/FileDetails";

const mapStateToProps = state => ({
  "selectedItems": state.repositoryState.selectedItems
});

export default connect(mapStateToProps)(FileDetails);
