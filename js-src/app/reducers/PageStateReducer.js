import update from "immutability-helper";
import {
  GOTO_NEXT_STEP,
  GOTO_PREVIOUS_STEP,
  GOTO_STEP
} from "../actions/index";
import { findStep } from "../utils/steps";

export default function (state = { "currentStep": "step1" }, action) {
  const currentStep = findStep(state.currentStep);

  switch (action.type) {
    case GOTO_NEXT_STEP: {
      const nextStep = currentStep.next().ID || currentStep; // go to next in the list
      return update(state, {
        /* eslint-disable */
        "currentStep": {$set: nextStep}
        /* eslint-enable */
      });
    }
    case GOTO_PREVIOUS_STEP: {
      const previousStep = currentStep.previous().ID || currentStep; // go to previous in the list
      return update(state, {
        /* eslint-disable */
        "currentStep": {$set: previousStep}
        /* eslint-enable */
      });
    }
    case GOTO_STEP: {
      return update(state, {
        /* eslint-disable */
        "currentStep": {$set: action.stepNumber}
        /* eslint-enable */
      });
    }
    default: {
      return state;
    }
  }
}
