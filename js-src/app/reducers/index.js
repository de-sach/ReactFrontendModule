import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import pageStateReducer from "./PageStateReducer";
import loginReducer from "./LoginReducer";
import repositoryReducer from "./RepositoryReducer";
import metadataReducer from "./MetadataReducer";

const AppReducer = combineReducers({
  "pageState": pageStateReducer,
  "form": formReducer,
  "loginState": loginReducer,
  "repositoryState": repositoryReducer,
  "metadata": metadataReducer
});

export default AppReducer;
