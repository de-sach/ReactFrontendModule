import update from "immutability-helper";
import {
  ADD_TO_SELECTED_ITEMS,
  CLEAR_SELECTED_ITEMS,
  REMOVE_FROM_SELECTED_ITEMS,
  UPDATE_CURRENT_FOLDER,
  UPDATE_CURRENT_PATH,
  UPDATEREPO
} from "../actions";

const initialState = {
  "repository": {
    "repositoryName": "initial repo name",
    "folder": null
  },
  "path": [],
  "selectedItems": []
};

export default function (state = initialState, action) {
  switch (action.type) {
    case UPDATEREPO: {
      return update(state, {
        /* eslint-disable */
        "repository": { $set: action.repository }
        /* eslint-enable */
      });
    }
    case UPDATE_CURRENT_FOLDER: {
      const newFolder = action.folder;
      return update(state, {
        /* eslint-disable */
        "folder": { $set: newFolder }
        /* eslint-enable */
      });
    }
    case ADD_TO_SELECTED_ITEMS: {
      const newSelectedItems = [];
      for (const index in state.selectedItems) {
        if (index) {
          newSelectedItems.push(state.selectedItems[index]);
        }
      }
      newSelectedItems.push(action.item);
      return update(state, {
        /* eslint-disable */
        "selectedItems": { $set: newSelectedItems }
        /* eslint-enable */
      });
    }
    case REMOVE_FROM_SELECTED_ITEMS: {
      const newSelectedItems = [];
      for (const index in state.selectedItems) {
        if (action.item.url !== state.selectedItems[index].url) {
          newSelectedItems.push(state.selectedItems[index]);
        }
      }
      return update(state, {
        /* eslint-disable */
        "selectedItems": { $set: newSelectedItems }
        /* eslint-enable */
      });
    }
    case CLEAR_SELECTED_ITEMS: {
      return update(state, {
        /* eslint-disable */
        "selectedItems": { $set: [] }
        /* eslint-enable */
      });
    }
    case UPDATE_CURRENT_PATH: {
      const newPath = [];
      let found = false;
      let index = 0;
      while (index < state.path.length && !found) {
        if (action.folder.childUrl !== state.path[index].childUrl) {
          newPath.push(state.path[index]);
        } else {
          found = true;
        }
        index += 1;
      }
      newPath.push(action.folder);
      return update(state, {
        /* eslint-disable */
        "path": { $set: newPath }
        /* eslint-enable */
      });
    }
    default: {
      return state;
    }
  }
}
