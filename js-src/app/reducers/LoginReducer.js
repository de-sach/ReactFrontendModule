import update from "immutability-helper";
import { LOGIN, STORE_TOKEN } from "../actions";

const initialState = {
  "loginState": false,
  "token": null
};
export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN: {
      return update(state, {
        /* eslint-disable */
        "loginState": { $set: action.status }
        /* eslint-enable */
      });
    }
    case STORE_TOKEN: {
      return update(state, {
        /* eslint-disable */
        "token": { $set: action.token }
        /* eslint-enable */
      });
    }
    default: {
      return state;
    }
  }
}
