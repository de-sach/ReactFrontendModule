import update from "immutability-helper";
import {
  ADD_TO_METADATA,
  EDIT_IN_METADATA,
  REMOVE_FROM_METADATA,
  SET_ALL_METADATA,
  SET_FILE_METADATA
} from "../actions";

const initialState = {
  "metadataFileList": [],
  "allMetadataList": []
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_TO_METADATA: {
      const newMetadata = state.metadataFileList;
      if (action.tag !== undefined && action.tag !== "") {
        let found = false;
        for (const index in newMetadata) {
          if (newMetadata[index].fileId === action.fileId) {
            found = true;
            newMetadata[index].metadata.push(action.tag);
          }
        }
        if (!found) {
          const fileItem = {
            "fileId": action.fileId,
            "metadata": []
          };
          fileItem.metadata.push(action.tag);
          newMetadata.push(fileItem);
        }
      }
      return update(state, {
        /* eslint-disable */
        "metadataFileList": { $set: newMetadata }
        /* eslint-enable */
      });
    }

    case REMOVE_FROM_METADATA: {
      const newMetadata = [];
      let newFileItem;
      for (const fileIndex in state.metadataFileList) {
        if (state.metadataFileList[fileIndex].fileId === action.fileId) {
          newFileItem = state.metadataFileList[fileIndex];
          const newMetadataList = [];
          for (const index in newFileItem.metadata) {
            if (newFileItem.metadata[index] !== action.tag) {
              newMetadataList.push(newFileItem.metadata[index]);
            }
          }
          newFileItem.metadata = newMetadataList;
          newMetadata.push(newFileItem);
        } else {
          newMetadata.push(state.metadataFileList[fileIndex]);
        }
      }
      return update(state, {
        /* eslint-disable */
        "metadataFileList": { $set: newMetadata }
        /* eslint-enable */
      });
    }
    case EDIT_IN_METADATA: {
      const newMetadata = [];
      let newFileItem;
      let updated = false;
      for (const fileIndex in state.metadataFileList) {
        if (state.metadataFileList[fileIndex].fileId === action.fileId) {
          newFileItem = state.metadataFileList[fileIndex];
          const newFileItemMetadata = [];
          for (const index in newFileItem.metadata) {
            if (newFileItem.metadata[index] === action.original && !updated) {
              newFileItemMetadata.push(action.newTag);
              // max update one tag at a time
              updated = true;
            } else {
              newFileItemMetadata.push(newFileItem.metadata[index]);
            }
          }
          newFileItem.metadata = newFileItemMetadata;
          newMetadata.push(newFileItem);
        } else {
          newMetadata.push(state.metadataFileList[fileIndex]);
        }
      }
      return update(state, {
        /* eslint-disable */
        "metadataFileList": { $set: newMetadata }
        /* eslint-enable */
      });
    }
    case SET_ALL_METADATA: {
      return update(state, {
        /* eslint-disable */
        "allMetadataList": { $set: action.metadata }
        /* eslint-enable */
      });
    }
    case SET_FILE_METADATA: {
      const newMetadata = [];
      state.metadataFileList.forEach((file) => {
        const newFile = file;
        if (file.fileId === action.fileId) {
          newFile.metadata = action.tagList;
        }
        newMetadata.push(newFile);
      });
      return update(state, {
        /* eslint-disable */
        "metadataFileList": { $set: newMetadata }
        /* eslint-enable */
      });
    }
    default: {
      return state;
    }
  }
}
