import React, { Component } from "react";
import contentManagementRequests from "../../utils/oauthContentManagementRequests";
import { middlewareConstants } from "../../constants/AtomPubConstants";
import RepositoryContainer from "../../containers/RepositoryContainer";
import LoginForm from "./Form";

const remoteUrl = "http://localhost:8080/"; // "http://content-repository-backend-dev3.eu-west-1.elasticbeanstalk.com/";


export default class LoginPage extends Component {

  loginpageLogin(username, password) {
    contentManagementRequests.Login(middlewareConstants.tokenUrl, username, password, (succes, response) => {
      if (succes) {
        const token = response.data;
        this.props.storeToken(token);
        contentManagementRequests.getRepositoryMiddleware(middlewareConstants.repositoryUrl, this.props.token.access_token, (repositorySucces, repositoryResponse) => {
          if (repositorySucces) {
            const data = repositoryResponse.data;
            this.props.updateRepository(data);
            this.props.setCurrentFolder(data.folder);
            this.props.setPath(data.folder);
            this.props.setLoginState(true);
          }
        });
        contentManagementRequests.getAllMetadataTagsMiddleware(this.props.token.access_token, (metadataSuccess, metadataResponse) => {
          if (metadataSuccess) {
            const metadata = metadataResponse.data.metadata;
            this.props.setAllMetadata(metadata);
          }
        });
      } else {
        this.props.setLoginState(false);
      }
    });
  }

  loginpageLogout() {
    this.props.setLoginState(false);
  }

  openAdminWebModule() {
    window.open(`${remoteUrl}admin`);
  }

  handleDrag(evt) {
    evt.preventDefault();
  }


  render() {
    const formState = [];
    const currentlySending = false;
    return (
      <div
        className="m-application__container"
        onDragEnter={e => this.handleDrag(e)}
        onDragOver={e => this.handleDrag(e)}
        onDrop={e => this.handleDrag(e)}
      >
        {
          this.props.loginState &&
          <div className="m-application__topbar row no-gutters">
            <button
              className="m-application__button col-sm-offset-10 col-sm-2"
              onClick={() => this.loginpageLogout()}>
              Logout
            </button>
            <button
              className="m-application__button col sm-offset-8 col-sm-2"
              onClick={() => this.openAdminWebModule()}>
              Edit Users
            </button>
          </div>
        }
        <div className="container m-application__content">
          <div className="row m-application__mainRow no-gutters">
            <div className="m-application col-sm-12 col-sm-offset-1 no-gutters">
              {this.props.loginState === false &&
              <LoginForm
                data={formState}
                dispatch={this.props.dispatch}
                history={this.props.history}
                onSubmit={(username, password) => {
                  this.loginpageLogin(username, password);
                }}
                btnText="Sign in"
                currentlySending={currentlySending} />}
              {
                this.props.loginState === true &&
                <div className="m-application__repository no-gutters">
                  <RepositoryContainer />
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    );

  }
}
