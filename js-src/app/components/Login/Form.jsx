import React, { Component } from 'react';
import PropTypes from 'prop-types';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "username": "",
      "password": ""
    };
  }

  // Change the username in the app state
  loginformChangeusername(evt) {
    this.setState({
      "username": evt.target.value
    });
  }

  // Change the password in the app state
  loginformChangepassword(evt) {
    this.setState({
      "password": evt.target.value
    });
  }


  // onSubmit call the passed onSubmit function
  loginformOnsubmit(evt) {
    evt.preventDefault();
    this.props.onSubmit(this.state.username, this.state.password);
  }

  render() {
    return (
      <form
        className="m-loginForm container-fluid"
        onSubmit={(e) => {
          this.loginformOnsubmit(e);
        }}
      >
        <div className="m-loginForm__header row">
          <div className="col-sm-12">
            Repository Viewer <br /><br /><br />
            Please sign in
          </div>
        </div>
        <div className="m-loginForm__username row">
          <label htmlFor="username" className="m-loginForm__fieldLabel col-sm-12">Username
            <input
              className="m-loginForm__input"
              type="text"
              id="username"
              value={this.state.username}
              placeholder="username"
              onChange={(e) => {
                this.loginformChangeusername(e);
              }}
              autoCorrect="off"
              autoCapitalize="off"
              spellCheck="false" />
          </label>
        </div>
        <div className="m-loginForm__password row">
          <label htmlFor="password" className="m-loginForm__fieldLabel col-sm-12">Password
            <input
              className="m-loginForm__input"
              id="password"
              type="password"
              value={this.state.password}
              placeholder="••••••••••"
              onChange={(e) => {
                this.loginformChangepassword(e);
              }} />
          </label>
        </div>
        <button className="m-loginForm__button" type="submit">{this.props.btnText}</button>
      </form>
    );
  }
}

LoginForm.propTypes = {
  "onSubmit": PropTypes.func.isRequired,
  "btnText": PropTypes.string.isRequired
};

export default LoginForm;
