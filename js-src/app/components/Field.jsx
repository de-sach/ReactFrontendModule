import React, { Component } from "react";
import { Field as ReduxField } from "redux-form";

class Field extends Component {
  render() {
    return (
      <ReduxField
        component={this.props.component}
        {...this.props} />
    );
  }
}

export default Field;
