import React, { Component } from "react";
import DetailContainer from "../../../containers/DetailContainer";

class FileDetails extends Component {

  render() {
    const fileDetailsList = [];
    const selectedItems = this.props.selectedItems;
    for (const index in selectedItems) {
      if (index) {
        const detailView = <DetailContainer key={index} file={selectedItems[index]} />;
        fileDetailsList.push(detailView);
      }
    }
    return (
      <div className="m-filedetail">
        <div className="row">
          <div className="m-filedetail__header col-sm-12">
            Selected:
          </div>
        </div>
        {fileDetailsList}
      </div>
    );
  }
}

export default FileDetails;
