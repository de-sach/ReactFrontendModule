/* Metadata tags are tags that are shown to the user upon selecting a file
 * each tag needs to have
 *    selected state -> keep (default)
 *    unselected state -> remove
 * edit tag option -> all tags become editable -> detail.jsx
 * add tag in Detail.jsx -> in edit
 * auto save changed tags in redux, on select other file/folder -> update
 */
import React, { Component } from "react";

class MetadataTag extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "tag": this.props.tag,
      "selected": true,
      "className": this.props.className
    };
  }

  toggleSelected() {
    // set class for visual
    if (this.state.selected) { // previous state
      this.setState({
        "className": this.props.className
      });
      this.props.removeFromMetadata(this.props.url, this.state.tag);
      this.props.bindReload();
    } else {
      this.setState({
        "className": `${this.props.className} selected`
      });
    }
    // set state
    this.setState({
      "selected": !this.state.selected
    });
  }

  updateTag(e) {
    this.props.editInMetadata(this.props.url, this.state.tag, e.target.value);
    this.setState({
      "tag": e.target.value
    });
  }

  render() {
    let renderObject = <div className={this.state.className}>{this.state.tag}</div>;
    if (!this.props.editable) {
      renderObject = (
        <div
          className={this.state.className}
          role="button"
          tabIndex={0}
        >
          {this.state.tag}
          {this.state.selected &&
          <button onClick={() => this.toggleSelected()}>
            -
          </button>}
        </div>
      );
    } else {
      renderObject = (
        <div className={this.state.className}>
          <input
            type="text"
            onChange={(e) => {
              this.updateTag(e);
            }}
            value={this.state.tag}
          />
        </div>
      );
    }
    return (renderObject);
  }
}

export default MetadataTag;
