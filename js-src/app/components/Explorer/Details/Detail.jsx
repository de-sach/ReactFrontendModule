import React, { Component } from "react";
import contentManagementRequests from "../../../utils/oauthContentManagementRequests";
import MetadataTagContainer from "../../../containers/MetadataTagContainer";

class Detail extends Component {
  constructor(props) {
    super(props);
    let originalMetadataTags = [];
    if (this.props.file.metadata !== undefined) {
      originalMetadataTags = this.props.file.metadata
        .slice(1, this.props.file.metadata.length - 1)
        .split(",");
    }
    originalMetadataTags.forEach((tag) => {
      this.props.addToMetadata(this.props.file.url, tag);
    });
    this.state = {
      "editable": false,
      "displayItemList": this.createDisplayItemListFromMetadataTags(originalMetadataTags, false),
      "metadata": this.getMetadataForFile()
    };
  }

  updateMetadataTags(editable) {
    const fileUrl = this.props.file.url;
    const newMetadata = this.getMetadataForFile();
    if (newMetadata.length > 0) {
      const metadataString = this.createMetadataStringFromMetadataTags(newMetadata);
      const newDisplayItemList = this.createDisplayItemListFromMetadataTags(newMetadata, editable);
      contentManagementRequests.updateMetadataMiddleware(metadataString, fileUrl, this.props.token, (success) => {
        if (!success) {
          alert("error");
        }
      });
      this.setState({
        "metadata": newMetadata,
        "displayItemList": newDisplayItemList
      });
    } else {
      console.log("metadat is not updated to empty tags");
    }
  }

  componentWillMount() {
    this.updateMetadataTags(this.state.editable);
  }

  toggleEditable() {
    this.updateMetadataTags(!this.state.editable);
    this.setState({
      "editable": !this.state.editable
    });
  }

  addToMetadata() {
    this.props.addToMetadata(this.props.file.url, "enter new tag");
    this.updateMetadataTags(this.state.editable);
  }

  render() {
    return (
      <div className="row">
        <div className="m-filedetail__detailItemTitle col-sm-12">
          {this.props.file.title}
        </div>
        <div className="m-fileDetail__button">
          <button onClick={() => this.toggleEditable()}>
            {this.state.editable ? "done" : "edit"}
          </button>
          {this.state.editable &&
          <button onClick={() => this.addToMetadata()}>add tag</button>
          }
        </div>
        {this.state.displayItemList}
      </div>
    );
  }

  createDisplayItemListFromMetadataTags(list, editable) {
    const newList = [];
    const originalList = this.clearDuplicateTagsFromList(list);
    for (const index in originalList) {
      if (list[index] !== "") {
        newList.push(<MetadataTagContainer
          className={"m-fileDetail__detailItemTagList col-sm-12"}
          key={index}
          index={index}
          tag={list[index]}
          editable={editable}
          bindReload={() => this.updateMetadataTags(editable)}
          url={this.props.file.url}
        />);
      }
    }
    return newList;
  }

  createMetadataStringFromMetadataTags(list) {
    let string = "[";
    if (list.length > 0) {
      for (const index in list) {
        if (index) {
          string += list[index];
          string += ",";
        }
      }
    }
    string += "]";
    return string;
  }

  getMetadataForFile() {
    let metadata = [];
    for (const fileIndex in this.props.metadataFileList) {
      if (this.props.metadataFileList[fileIndex].fileId === this.props.file.url) {
        metadata = this.props.metadataFileList[fileIndex].metadata;
      }
    }
    return metadata;
  }

  clearDuplicateTagsFromList(list) {
    const newList = [];
    list.forEach((item) => {
      if (newList.indexOf(item) <= -1) {
        newList.push(item);
      }
    });
    console.log(`list length shortened from ${list.length} to ${newList.length}`);
    this.props.setFileMetadata(this.props.file.url, newList);
    return newList;
  }
}

export default Detail;
