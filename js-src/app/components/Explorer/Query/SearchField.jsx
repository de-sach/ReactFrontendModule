import React, { Component } from "react";
import { connect } from "react-redux";
import { setCurrentFolder } from "../../../actions";
import contentManagementRequests from "../../../utils/oauthContentManagementRequests";

class SearchField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "search": "",
      "searched": false,
      "searchType": "metadata tags"
    };
  }

  render() {
    return (
      <div className="m-search__container col-sm-10">
        <div className="row">
          <input
            className="m-search__input col-sm-4"
            id="search"
            type="text"
            placeholder="type to search"
            value={this.state.search}
            onChange={(e) => {
              this.changeSearch(e);
            }} />
          <div className="m-search__doSearch col-sm-2">
            <button
              onClick={(e) => {
                this.search(e);
              }}>
              Search
            </button>
          </div>
          <div className="m-search__searchType col-sm-5 col-sm-offset-1">
            name
            <input
              type="range"
              min="1"
              max="2" // onChange={() => this.changeSearchType()}
              value={(this.state.searchType === "metadata tags") ? 2 : 1}
              onClick={() => this.changeSearchType()}
              onChange={() => this.pass()}
            />
            metadata
          </div>
        </div>
        {this.state.searched &&
        <div className="row">
          <div className="m-search__searchedHeader col-sm-12">
            Search results for: {this.state.search}
          </div>
        </div>
        }
      </div>
    );
  }

  changeSearch(e) {
    this.setState({
      "search": e.target.value.trim()
    });
  }

  pass() {

  }

  search(e) {
    e.preventDefault();
    console.log(this.state.search);
    contentManagementRequests.searchMiddleware(this.state.search, this.props.token, this.state.searchType, (success, response) => {
      console.log(response);
      if (success) {
        const folder = response.data;
        this.props.setCurrentFolder(folder);
        this.setState({
          "searched": true
        });
      }
    });

  }

  changeSearchType() {
    if (this.state.searchType === "metadata tags") {
      this.setState({
        "searchType": "name"
      });
    }
    if (this.state.searchType === "name") {
      this.setState({
        "searchType": "metadata tags"
      });
    }
  }
}

const mapStateToProps = state => ({
  "token": state.loginState.token.access_token
});
const mapDispatchToProps = dispatch => ({
  "setCurrentFolder": folder => dispatch(setCurrentFolder(folder))
});
export default connect(mapStateToProps, mapDispatchToProps)(SearchField);
