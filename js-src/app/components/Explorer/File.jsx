import React, { Component } from "react";
import { ContextMenuTrigger } from "react-contextmenu";
import ContextMenuContainer from "../../containers/ContextMenuContainer";

class File extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "isSelected": false
    };
  }

  fileClick() {
    this.setState({
      "isSelected": false
    });
    this.props.removeFromSelected(this.props.file);
    this.props.onClick(this.props.file);
  }

  toggleSelected() {
    if (this.state.isSelected) {
      this.props.removeFromSelected(this.props.file);
    } else {
      this.props.addToSelected(this.props.file);
    }
    this.setState({
      "isSelected": !this.state.isSelected
    });

  }


  render() {
    return (
      <div className="col-sm-3 m-file__item">
        <ContextMenuTrigger id="file">
          <div
            className={this.state.isSelected ? "col-sm-12 m-file__file--selected" : "col-sm-12 m-file__file"}
            onDoubleClick={() => this.fileClick()}
            onClick={() => this.toggleSelected()}
            role="button"
            tabIndex={0}
          >
            <div className="m-file__fileText">
              {this.props.file.title}
            </div>
          </div>
        </ContextMenuTrigger>
        <ContextMenuContainer file={this.props.file} />
      </div>
    );
  }
}

export default File;
