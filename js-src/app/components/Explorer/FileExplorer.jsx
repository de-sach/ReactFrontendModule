import React, { Component } from "react";
import Edit from "./EditOptions/EditExplorer";
import FolderContainer from "../../containers/FolderContainer";
import FileContainer from "../../containers/FileContainer";
import { AtomPubConstants } from "../../constants/AtomPubConstants";
import { stringReplacer } from "../../utils/stringReplacer";
import contentManagementRequests from "../../utils/oauthContentManagementRequests";

class FileExplorer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "children": null
    };
  }

  openFolder(folder) {
    this.props.clearSelectedItems();
    let newFolderUrl = folder.url;
    newFolderUrl = stringReplacer(newFolderUrl, "entry", "children");
    // console.log(`folder url:${newFolderUrl}`);
    contentManagementRequests.getFolderMiddleware(newFolderUrl, this.props.token, (succes, response) => {
      if (succes === true) {
        const newFolder = response.data;
        this.props.setCurrentFolder(newFolder);
        this.props.setCurrentPath(newFolder);
      }
    });
  }

  openFile(file) {
    this.props.clearSelectedItems();
    const baseUrl = AtomPubConstants.baseUrl;
    const fileName = file.title;
    const fileId = file.url.split("=")[1];
    const completeUrl = `${baseUrl}content/${fileName}?id=${fileId}&access_token=${this.props.token}`;
    // console.log(completeUrl);
    window.open(completeUrl);
  }

  render() {
    const folder = this.props.folder;
    const childFolderList = [];
    const childItemList = [];
    const folderRenderList = [];
    const fileRenderList = [];
    if (folder !== null && folder !== undefined) {
      for (let count = 0; count < folder.children.length; count += 1) {
        const folderItem = folder.children[count];
        if (folderItem.type === "cmis:folder") {
          childFolderList.push(folderItem);
        } else if (folderItem.type === "cmis:document" || folderItem.type === "extcmis:pdf_document") {
          childItemList.push(folderItem);
        }
      }
      // console.log(childFolderList);
      // console.log(childItemList);
      for (let count = 0; count < childFolderList.length; count += 1) {
        const renderFolder = childFolderList[count];
        if (renderFolder !== null) {
          folderRenderList.push(
            <FolderContainer
              folder={renderFolder}
              onClick={(f) => {
                this.openFolder(f);
              }}
              key={count}
            />
          );
        }
      }
      for (let count = 0; count < childItemList.length; count += 1) {
        const renderFile = childItemList[count];
        if (renderFile !== null) {
          fileRenderList.push(
            <FileContainer
              file={renderFile}
              onClick={(f) => {
                this.openFile(f);
              }}
              key={count}
            />
          );
        }
      }
    }
    /* eslint-disable */
    return (
      <div className="m-file container">
        <Edit />
        <div className="m-file__rows">
          <div className="row m-file__row">
            {folderRenderList}
          </div>
          <div className="row m-file__row">
            {fileRenderList}
          </div>
        </div>
      </div>
    );
    /* eslint-enable */
  }
}

export default FileExplorer;
