import React, { Component } from "react";
import { ContextMenu, MenuItem } from "react-contextmenu";
import { stringReplacer } from "../../../utils/stringReplacer";
import contentManagementRequests from "../../../utils/oauthContentManagementRequests";

class FileContextMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "file": this.props.file,
      "editing": false,
      "title": this.props.file.title
    };
  }

  renameFile() {
    this.showFileNamePopup();
  }

  showFileNamePopup() {
    this.setState({
      "editing": true
    });
  }


  editTitle(evt) {
    this.setState({
      "title": evt.target.value
    });
  }

  saveTitle() {
    const title = this.state.title;
    if (title !== "") {
      this.setState({
        "editing": false
      });
      const originalTitle = this.props.file.title;
      if (originalTitle !== title) {
        console.log(this.props.file);
        contentManagementRequests.updateFileNameMiddleware(this.props.file.url, this.props.token, this.state.title, (success, response) => {
          if (success) {
            console.log(response);
            this.updateCurrentFolder();
          }
        });
      }
    } else {
      this.setState({
        "editing": false
      });
    }
  }

  updateCurrentFolder() {
    const newFolderUrl = stringReplacer(this.props.folder.childUrl, "entry", "children");
    contentManagementRequests.getFolderMiddleware(newFolderUrl, this.props.token, (succes, response) => {
      if (succes === true) {
        const myFolder = response.data;
        this.props.setCurrentFolder(myFolder);
      }
    });
  }

  deleteFile() {
    let itemId = this.props.file.url.split("?")[1];
    itemId = itemId.slice(3, itemId.length);
    contentManagementRequests.deleteObjectMiddleware(itemId, this.props.token, (success) => {
      if (success) {
        this.updateCurrentFolder();
      }
    });
  }

  render() {
    return (
      <div className="m-contextMenu">
        <ContextMenu id="file">
          <MenuItem onClick={() => this.renameFile()}>
            Rename
          </MenuItem>
          <MenuItem onClick={() => this.deleteFile()}>
            Delete
          </MenuItem>
        </ContextMenu>
        {
          this.state.editing &&
          <div className="m-contextMenu__renamePopup">
            Enter the new title for this file
            <label htmlFor="title">
              <input
                type="text"
                id="title"
                placeholder={this.state.file.title}
                onChange={e => this.editTitle(e)}
              />
            </label>
            <button onClick={() => this.saveTitle()}>
              Done
            </button>
          </div>
        }
      </div>
    );
  }
}

/* eslint-disable */
/*
{
  this.state.editing &&
  <div className="m-contextMenu__renamePopup">
    Enter the new title for this file
    <label htmlFor="title">
      <input
        type="text"
        id="title"
        onChange={e => this.editTitle(e)}
      />
    </label>
    <button onClick={() => this.saveTitle()}>
      Done
    </button>
  </div>
}
/* eslint-enable */

export default FileContextMenu;
