import React, { Component } from "react";
import contentManagementRequests from "../../utils/oauthContentManagementRequests";
import { stringReplacer } from "../../utils/stringReplacer";

class Path extends Component {

  openFolder = (pathItem) => {
    const newFolderUrl = stringReplacer(pathItem.childUrl, "entry", "children");
    contentManagementRequests.getFolderMiddleware(newFolderUrl, this.props.token, (success, response) => {
      if (success === true) {
        const myFolder = response.data;
        this.callReducers(myFolder);
      }
    });
  };

  callReducers(folder) {
    this.props.setCurrentFolder(folder);
    this.props.setCurrentPath(folder);
  }

  render() {
    const locationArray = [];
    // TODO: update current location
    if (this.props.path !== null && this.props.path !== undefined) {
      for (let count = 0; count < this.props.path.length; count += 1) {
        locationArray.push(
          <div
            className="m-path__item col-sm-2"
            onClick={() => this.openFolder(this.props.path[count])}
            role="button"
            key={count}
            tabIndex={0}
          >
            {`${this.props.path[count].folderTitle}`}
          </div>
        );
      }
    }
    return (
      <div className="m-path container-fluid">
        <div className="row">
          {locationArray}
        </div>
      </div>);
  }
}

export default Path;
