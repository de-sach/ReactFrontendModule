import React, { Component } from "react";
import contentManagementRequests from "../../../utils/oauthContentManagementRequests";

class AddDocument extends Component {
  constructor(props) {
    super(props);
    // console.log(folder);
    this.state = {
      "createFolder": false,
      "folderName": null
    };
  }

  doClick() {
    this.setState({
      "createFolder": !this.state.createFolder
    });
  }

  updateCurrentFolder() {
    contentManagementRequests.getFolderMiddleware(this.props.folder.childUrl, this.props.token, (succes, response) => {
      if (succes === true) {
        const newFolder = response.data;
        console.log(response.data);
        this.props.setCurrentFolder(newFolder);
        this.setState({
          "createFolder": false
        });
      }
    });
  }

  setFolderName(evt) {
    this.setState({
      "folderName": evt.target.value
    });
  }

  createFolder() {
    contentManagementRequests.createFolderMiddleware(this.props.folder.childUrl, this.state.folderName, this.props.token, (success) => {
      if (success) {
        this.updateCurrentFolder();
      }
    });
  }

  render() {
    return (
      <div className="m-edit__addFolder col-sm-3">
        <button className="add_folder" onClick={() => this.doClick()}>
          Add Folder
        </button>
        {this.state.createFolder &&
        <div>
          <label htmlFor="folderName">
            <input
              type="text"
              id="folderName"
              onChange={(e) => {
                this.setFolderName(e);
              }}
            />
          </label>
          <button type="submit" onClick={() => this.createFolder()}>
            submit
          </button>
        </div>
        }
      </div>
    );
  }
}

export default AddDocument;
