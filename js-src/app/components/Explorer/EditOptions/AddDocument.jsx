import React, { Component } from "react";
import UploadField from "./UploadField";
import { stringReplacer } from "../../../utils/stringReplacer";
import contentManagementRequests from "../../../utils/oauthContentManagementRequests";

class AddDocument extends Component {
  constructor(props) {
    super(props);
    // console.log(folder);
    this.state = {
      "wantToUpload": false,
      "addDocumentText": "Add Document"
    };
  }

  doClick() {
    // console.log("in doClick");
    // this.state.wantToUpLoad = true;
    let newText = "";
    if (this.state.addDocumentText === "Add Document") {
      newText = "Cancel";
    } else {
      newText = "Add Document";
    }
    this.setState({
      "wantToUpload": !this.state.wantToUpload,
      "addDocumentText": newText
    });
    // console.log(this.state.wantToUpload);
  }

  updateCurrentFolder() {
    const newFolderUrl = stringReplacer(this.props.folder.childUrl, "entry", "children");
    contentManagementRequests.getFolderMiddleware(newFolderUrl, this.props.token, (succes, response) => {
      if (succes === true) {
        const myFolder = response.data;
        this.props.setCurrentFolder(myFolder);
      }
    });
  }

  handleUploadedFile(response) {
    // process metadata
    const stringPropertiesList = response.data["atom:entry"]["cmisra:object"][0]["cmis:properties"][0]["cmis:propertyString"];
    for (const item in stringPropertiesList) {
      if (item) {
        const property = stringPropertiesList[item];
        if (property.$.propertyDefinitionId === "cmis:description") {
          if (property["cmis:value"] !== undefined) {
            this.addMetadataToDocument(property["cmis:value"][0]);
          }
        }
      }
    }
    this.updateCurrentFolder();
    this.setState({
      "wantToUpload": false,
      "addDocumentText": "Add Document"
    });
  }

  addMetadataToDocument(metadataListAsString) {
    const newString = metadataListAsString.slice(1, -1);
    const metadataList = newString.split(",");
    console.log(metadataList);
  }

  render() {
    return (
      <div className={this.state.wantToUpload ? "m-edit__uploadFile" : "m-edit_addDocument col-sm-3"}>
        <button className="add_document" onClick={() => this.doClick()}>
          {this.state.addDocumentText}
        </button>
        {this.state.wantToUpload &&
        <UploadField
          folder={this.props.folder}
          onUpload={(response) => {
            this.handleUploadedFile(response);
          }}
          token={this.props.token}
        />}
      </div>
    );
  }
}

export default AddDocument;
