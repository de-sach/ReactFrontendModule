import React, { Component } from "react";
import contentManagementRequests from "../../../utils/oauthContentManagementRequests";

class DeleteElement extends Component {
  doClick() {
    const selectedItems = this.props.selectedItems;
    for (const itemIndex in selectedItems) {
      if (itemIndex) {
        if (selectedItems[itemIndex].title !== undefined) { // already deleted for some reason
          let itemId = selectedItems[itemIndex].url.split("?")[1];
          itemId = itemId.slice(3, itemId.length);
          contentManagementRequests.deleteObjectMiddleware(itemId, this.props.token, (success) => {
            if (success) {
              this.updateCurrentFolder();
            }
          });
        }
      }
    }
    this.props.clearSelectedItems();
  }

  updateCurrentFolder() {
    contentManagementRequests.getFolderMiddleware(this.props.folder.childUrl, this.props.token, (succes, response) => {
      if (succes === true) {
        const newFolder = response.data;
        newFolder.url = this.props.folder.url;
        this.props.setCurrentFolder(newFolder);
      }
    });
  }

  render() {
    return (
      <div className="m-edit__delete col-sm-3">
        <button className="delete" onClick={() => this.doClick()}>
          Delete
        </button>
      </div>
    );
  }
}

export default DeleteElement;
