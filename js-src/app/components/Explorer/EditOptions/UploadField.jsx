import React, { Component } from "react";
import contentManagementRequests from "../../../utils/oauthContentManagementRequests";

class UploadField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "files": [],
      "uploadText": "click to select files or drag and drop"
    };
    // this.submit = this.submit.bind(this);
    // this.changeFiles = this.changeFiles.bind(this);
  }

  submit(evt) {
    evt.preventDefault();
    // console.log(`folder url:${this.props.folderUrl}`);
    contentManagementRequests.createDocumentsMiddleware(this.props.folder.childUrl, this.state.files, this.props.token, (succes, response) => {
      console.log(this.props.folder);
      if (succes) {
        this.props.onUpload(response);
      }
    });
  }

  changeFiles(evt) {
    const files = evt.target.files;
    let newUploadText = "";
    for (let index = 0; index < files.length; index += 1) {
      newUploadText += `${files[index].name}, `;
    }
    this.setState({
      "files": Array.from(files),
      "uploadText": newUploadText
    });
  }

  handleDrop(evt) {
    const files = evt.dataTransfer.files;
    let newUploadText = "";
    for (let index = 0; index < files.length; index += 1) {
      newUploadText += `${files[index].name}, `;
    }
    this.setState({
      "files": Array.from(files),
      "uploadText": newUploadText
    });
  }

  render() {
    return (
      <form onSubmit={(e) => {
        this.submit(e);
      }}>
        <div
          onDrop={e => this.handleDrop(e)}
        >
          <label htmlFor="files">
            <input
              type="file"
              id="files"
              onChange={(e) => {
                this.changeFiles(e);
              }}
              multiple="multiple" />

            {this.state.uploadText}
          </label>
          <button type="submit">
            upload
          </button>
        </div>
      </form>
    );
  }
}

export default UploadField;
