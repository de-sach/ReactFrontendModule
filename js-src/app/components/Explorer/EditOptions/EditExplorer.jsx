import React, { Component } from "react";
import AddDocumentContainer from "../../../containers/AddDocumentContainer";
import AddFolderContainer from "../../../containers/AddFolderContainer";
import DeleteElementContainer from "../../../containers/DeleteElementContainer";

class Edit extends Component {
  render() {
    return (
      <div className="m-edit row">
        <AddDocumentContainer />
        <AddFolderContainer />
        <DeleteElementContainer />
      </div>
    );
  }
}

export default Edit;
