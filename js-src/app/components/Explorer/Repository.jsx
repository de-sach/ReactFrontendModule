import React, { Component } from "react";
import PathContainer from "../../containers/PathContainer";
import FileExplorerContainer from "../../containers/FileExplorerContainer";
import FileDetailsContainer from "../../containers/FileDetailsContainer";
import SearchField from "./Query/SearchField";
import FilterTagListContainer from "../../containers/FilterTagListContainer";

class Repository extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "selectedOption": true,
      "details": false
    };
  }

  toggleSearch() {
    this.setState({
      "selectedOption": !this.state.selectedOption
    });
  }

  toggleDetails() {
    this.setState({
      "details": !this.state.details
    });
  }

  render() {
    return (
      <div className="m-repository container-fluid">
        <div className="m-repository__header row">
          <div className="col-sm-12">
            Welcome to the {this.props.repository.repositoryName}
          </div>
        </div>
        <div className="row">
          <div className="m-repository__query col-sm-12">
            <button
              className="m-repository__queryMode col-sm-12"
              onClick={() => this.toggleSearch()}>
              {this.state.selectedOption ? "filter" : "search"} mode
            </button>
            {
              this.state.selectedOption &&
              <SearchField />
            }
            {
              !this.state.selectedOption &&
              <FilterTagListContainer />
            }
          </div>
        </div>
        <div className="row">
          <div className="m-repository__options col-sm-12">
            <PathContainer />
            <button onClick={() => this.toggleDetails()}>
              Details
            </button>
          </div>
        </div>
        <div className="m-repository__fileview row">
          <div
            className={this.state.details ? "m-repository__fileExplorer col-sm-9" : "m-repository__fileExplorer col-sm-12"}
          >
            <FileExplorerContainer />
          </div>
          {this.state.details &&
          <div className={"m-repository__detailsContainer col-sm-3"}>
            <FileDetailsContainer />
          </div>
          }
        </div>
      </div>
    );
  }
}

export default Repository;
