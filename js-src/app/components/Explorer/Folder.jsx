import React, { Component } from "react";
import { ContextMenuTrigger } from "react-contextmenu";
import ContextMenuContainer from "../../containers/ContextMenuContainer";

class Folder extends Component {
  constructor(props) {
    super(props);
    // this.folderClick = this.folderClick.bind(this);
    // this.toggleSelected = this.toggleSelected.bind(this);
    this.state = {
      "isSelected": false
    };
  }

  folderClick() {
    this.setState({
      "isSelected": false
    });
    this.props.onClick(this.props.folder);
  }

  toggleSelected() {
    if (this.state.isSelected) { // was selected should be unselected
      this.props.removeFromSelected(this.props.folder);
    } else {
      this.props.addToSelected(this.props.folder);
    }
    this.setState({
      "isSelected": !this.state.isSelected
    });
  }

  render() {
    return (
      <div className="col-sm-3 m-file__item">
        <ContextMenuTrigger id="file">
          <div
            className={this.state.isSelected ? "col-sm-12 m-file__folder--selected" : "col-sm-12 m-file__folder"}
            onDoubleClick={() => this.folderClick()}
            onClick={() => this.toggleSelected()}
            role="button"
            tabIndex={0}>
            <div className="m-file__folderText">
              {this.props.folder.title}
            </div>
          </div>
        </ContextMenuTrigger>
        <ContextMenuContainer file={this.props.folder} />
      </div>
    );
  }
}

export default Folder;
