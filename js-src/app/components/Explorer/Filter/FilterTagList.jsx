import React, { Component } from "react";
import contentManagementRequests from "../../../utils/oauthContentManagementRequests";

class FilterTagList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "selected": ""
    };
  }

  filterItems() {
    const searchMode = "metadata tags";
    contentManagementRequests.searchMiddleware(this.state.selected, this.props.token, searchMode, (success, response) => {
      console.log(response);
      if (success) {
        const folder = response.data;
        this.props.setCurrentFolder(folder);
      }
    });
  }

  getMetadataRenderList() {
    const returnList = [];
    for (const index in this.props.metadata) {
      if (index) {
        returnList.push(
          <div key={index} className="m-filter__item">
            <input
              type="radio"
              name="filterItem"
              key={index}
              value={this.props.metadata[index]}
              onClick={() => {
                this.setState({
                  "selected": this.props.metadata[index]
                });
                console.log(this.props.metadata[index]);
              }} />
            {this.props.metadata[index]}
          </div>
        );
      }
    }
    return returnList;
  }

  render() {
    return (
      <div className="m-filter__container row">
        <div className="m-filter__itemList col-sm-10">
          {this.getMetadataRenderList()}
        </div>
        <button className="m-filter__filterButton col-sm-2" onClick={() => this.filterItems()}>
          Filter
        </button>
      </div>
    );
  }
}

export default FilterTagList;
