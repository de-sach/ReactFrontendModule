import React, { Component } from "react";
import { reduxForm } from "redux-form";
import { validateAllFields } from "../utils/validation";

import Step1 from "../routes/Step1.jsx";
import Step2 from "../routes/Step2.jsx";
import Step3 from "../routes/Step3.jsx";

class App extends Component {
  render() {
    const { currentStep } = this.props;

    return (
      <div className="main-app">
        <div className="main-content">
          {currentStep === "step1" &&
          <Step1 />}
          {currentStep === "step2" &&
          <Step2 />}
          {currentStep === "step3" &&
          <Step3 />}
        </div>

        <button onClick={() => this.props.gotoPreviousStep()}>Previous step</button>
        <button onClick={() => this.props.gotoStep("step2")}>Go to step 2</button>
        <button onClick={() => this.props.gotoNextStep()}>Next step</button>
      </div>
    );
  }
}

const AppWrapper = reduxForm({
  "form": "app", // a unique name for this form,
  "validate": validateAllFields
})(App);

export default AppWrapper;
