const cmisConst = {
  "createDocument": {
    "cmisaction": "createDocument",
    "propertyId[0]": "cmis:objectTypeId",
    "propertyValue[0]": "cmis:document",
    "propertyId[1]": "cmis:name",
    "propertyValue[1]": "default name",
    "content": "default content"
  },
  "createFolder": {
    "cmisaction": "createFolder",
    "propertyId[0]": "cmis:objectTypeId",
    "propertyValue[0]": "cmis:folder",
    "propertyId[1]": "cmis:name",
    "propertyValue[1]": "default name"
  },
  "deleteItem": {
    "cmisaction": "delete",
    "allVersions": true
  }
};

export default cmisConst;
