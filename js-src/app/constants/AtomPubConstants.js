// const localUrl = "http://localhost:8080/";
const remoteUrl = "http://content-repository-backend-dev3.eu-west-1.elasticbeanstalk.com/";

export const AtomPubConstants = {

  "baseUrl": `${remoteUrl}cm/atom11/default/`,
  "searchUrl": `${remoteUrl}cm/atom11/default/query`,
  "Root collection": "children",
  "Types collection": "types",
  "Query collection": "query",
  "Checked out collection": "checkedout",
  "Unfiled collection": "unfiled",
  "Bulk update collection": "update",
  "createFolderTemplate": "<?xml version='1.0' encoding='UTF-8'?><atom:entry xmlns:atom='http://www.w3.org/2005/Atom' xmlns:cmis='http://docs.oasis-open.org/ns/cmis/core/200908/' xmlns:cmisra='http://docs.oasis-open.org/ns/cmis/restatom/200908/'><atom:id>urn:uuid:00000000-0000-0000-0000-00000000000</atom:id><atom:title>template1</atom:title><atom:updated>2018-04-17T18:29:45.781Z</atom:updated><cmisra:object><cmis:properties><cmis:propertyString propertyDefinitionId='cmis:name'><cmis:value>template1</cmis:value></cmis:propertyString><cmis:propertyId propertyDefinitionId='cmis:objectTypeId'><cmis:value>cmis:folder</cmis:value></cmis:propertyId></cmis:properties></cmisra:object></atom:entry>", // template 1
  "createDocumentTemplate": "<?xml version='1.0' encoding='UTF-8'?><atom:entry xmlns:atom='http://www.w3.org/2005/Atom' xmlns:cmis='http://docs.oasis-open.org/ns/cmis/core/200908/' xmlns:cmisra='http://docs.oasis-open.org/ns/cmis/restatom/200908/' xmlns:chemistry='http://chemistry.apache.org/'><atom:id>urn:uuid:00000000-0000-0000-0000-00000000000</atom:id><atom:title>template1</atom:title><atom:updated>2018-04-18T11:07:46.481Z</atom:updated><cmisra:content><cmisra:mediatype>template3</cmisra:mediatype><chemistry:filename>template1</chemistry:filename><cmisra:base64>template2</cmisra:base64></cmisra:content><cmisra:object><cmis:properties><cmis:propertyString propertyDefinitionId='cmis:name'><cmis:value>template1</cmis:value></cmis:propertyString><cmis:propertyString propertyDefinitionId='cmis:description'><cmis:value>template5</cmis:value></cmis:propertyString><cmis:propertyId propertyDefinitionId='cmis:objectTypeId'><cmis:value>template4</cmis:value></cmis:propertyId></cmis:properties></cmisra:object></atom:entry>", // template1, template2, template3, template4, template5
  "documentTypes": {
    "document": "cmis:document",
    "pdf": "extcmis:pdf_document"
  },
  "updateDocumentTemplate": "<?xml version='1.0' encoding='UTF-8'?><atom:entry xmlns:atom='http://www.w3.org/2005/Atom' xmlns:cmis='http://docs.oasis-open.org/ns/cmis/core/200908/' xmlns:cmisra='http://docs.oasis-open.org/ns/cmis/restatom/200908/'><cmisra:object><cmis:properties><cmis:propertyString propertyDefinitionId='cmis:description'><cmis:value>template</cmis:value></cmis:propertyString></cmis:properties></cmisra:object></atom:entry>", // template
  "searchTagTemplate": "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><cmis:query xmlns:app='http://www.w3.org/2007/app' xmlns:atom='http://www.w3.org/2005/Atom' xmlns:cmis='http://docs.oasis-open.org/ns/cmis/core/200908/' xmlns:cmism='http://docs.oasis-open.org/ns/cmis/messaging/200908/' xmlns:cmisra='http://docs.oasis-open.org/ns/cmis/restatom/200908/'><cmis:statement>SELECT cmis:name FROM cmis:document WHERE cmis:description = 'template'</cmis:statement><cmis:searchAllVersions>false</cmis:searchAllVersions><cmis:includeAllowableActions>false</cmis:includeAllowableActions><cmis:includeRelationships>none</cmis:includeRelationships><cmis:renditionFilter>*</cmis:renditionFilter><cmis:maxItems>50</cmis:maxItems><cmis:skipCount>0</cmis:skipCount></cmis:query>", // template
  "searchNameTemplate": "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><cmis:query xmlns:app='http://www.w3.org/2007/app' xmlns:atom='http://www.w3.org/2005/Atom' xmlns:cmis='http://docs.oasis-open.org/ns/cmis/core/200908/' xmlns:cmism='http://docs.oasis-open.org/ns/cmis/messaging/200908/' xmlns:cmisra='http://docs.oasis-open.org/ns/cmis/restatom/200908/'><cmis:statement>SELECT cmis:name FROM cmis:document WHERE cmis:name = 'template'</cmis:statement><cmis:searchAllVersions>false</cmis:searchAllVersions><cmis:includeAllowableActions>false</cmis:includeAllowableActions><cmis:includeRelationships>none</cmis:includeRelationships><cmis:renditionFilter>*</cmis:renditionFilter><cmis:maxItems>50</cmis:maxItems><cmis:skipCount>0</cmis:skipCount></cmis:query>",
  "getAllMetadataTagsTemplate": "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><cmis:query xmlns:app='http://www.w3.org/2007/app' xmlns:atom='http://www.w3.org/2005/Atom' xmlns:cmis='http://docs.oasis-open.org/ns/cmis/core/200908/' xmlns:cmism='http://docs.oasis-open.org/ns/cmis/messaging/200908/' xmlns:cmisra='http://docs.oasis-open.org/ns/cmis/restatom/200908/'><cmis:statement>SELECT cmis:name FROM extcmis:pdf_document </cmis:statement><cmis:searchAllVersions>false</cmis:searchAllVersions><cmis:includeAllowableActions>false</cmis:includeAllowableActions><cmis:includeRelationships>none</cmis:includeRelationships><cmis:renditionFilter>*</cmis:renditionFilter><cmis:maxItems>50</cmis:maxItems><cmis:skipCount>0</cmis:skipCount></cmis:query>",
  "updateFileNameTemplate": "<?xml version='1.0' encoding='UTF-8'?><atom:entry xmlns:atom='http://www.w3.org/2005/Atom' xmlns:cmis='http://docs.oasis-open.org/ns/cmis/core/200908/' xmlns:cmisra='http://docs.oasis-open.org/ns/cmis/restatom/200908/'><cmisra:object><cmis:properties><cmis:propertyString propertyDefinitionId='cmis:name'><cmis:value>template</cmis:value></cmis:propertyString></cmis:properties></cmisra:object></atom:entry>" // template
};
export const Oauth2TokenConstants = {
  "baseUrl": `${remoteUrl}oauth/token`,
  "parameters": {
    "client_id": "formula",
    "client_secret": "RHVSR4fTWTScB5Pz",
    "grant_type": "password",
    "username": "placeholder",
    "password": "placeholder"
  }
};
export const middlewareConstants = {
  "tokenUrl": "/token",
  "repositoryUrl": "/getRepository",
  "createFolderUrl": "/createFolder",
  "createDocumentUrl": "/createDocument",
  "getFolder": "/getFolder",
  "deleteObject": "/delete",
  "updateMetadata": "/update",
  "search": "/search",
  "getAllMetadataUrl": "/getAllMetadata",
  "updateFileName": "/updateFileName"
};

